install:
	docker-compose exec php composer install

start:
	docker-compose up -d

build:
	docker-compose up --build

stop:
	docker-compose down

clear_cache:
	docker-compose exec php bin/console c:c --env=dev && docker-compose exec php bin/console c:c --env=prod

entity:
	docker-compose exec php bin/console make:entity

entity_api:
	docker-compose exec php bin/console make:entity --api-resource

migrate:
	docker-compose exec php bin/console doctrine:migrations:migrate

dsu:
	docker-compose exec php bin/console doctrine:schema:update --force

ddd:
	docker-compose exec php bin/console d:d:d --force

ddc:
	docker-compose exec php bin/console d:d:c

set_data:
	docker-compose exec php bin/console data:departments && \
	docker-compose exec php bin/console data:days && \
	docker-compose exec php bin/console data:appointments-status && \
	docker-compose exec php bin/console data:shift-types