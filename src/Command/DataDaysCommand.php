<?php

namespace App\Command;

use App\Entity\Day;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DataDaysCommand extends Command
{
    protected static $defaultName = 'data:days';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this->setDescription('Command to set days');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->title('Setting days...');

        $dayRepository = $this->em->getRepository(Day::class);

        foreach(Day::DAYS as $dayName) {
            $day = $dayRepository->findOneBy([
                'name' => $dayName
            ]);

            if ($day) {
                continue;
            }

            else {
                $day = new Day();
                $day->setName($dayName);
                $this->em->persist($day);
            }
        }

        $this->em->flush();

        $io->success("Days set successfully !");

        return Command::SUCCESS;
    }
}
