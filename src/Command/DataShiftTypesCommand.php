<?php

namespace App\Command;

use App\Entity\TypeOfShift;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DataShiftTypesCommand extends Command
{
    protected static $defaultName = 'data:shift-types';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this->setDescription('Command to set types of shift');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->title('Setting types of shift...');

        $typeOfShiftRepository = $this->em->getRepository(TypeOfShift::class);

        foreach(TypeOfShift::TYPES_OF_SHIFT as $typeOfShiftName) {
            $typeOfShift = $typeOfShiftRepository->findOneBy([
                'name' => $typeOfShiftName
            ]);

            if ($typeOfShift) {
                continue;
            }

            else {
                $typeOfShift = new TypeOfShift();
                $typeOfShift->setName($typeOfShiftName);
                $this->em->persist($typeOfShift);
            }
        }

        $this->em->flush();

        $io->success("Types of shift set successfully !");

        return Command::SUCCESS;
    }
}
