<?php

namespace App\Command;

use App\Entity\Department;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Reader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DataDepartmentsCommand extends Command
{
    protected static $defaultName = 'data:departments';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this->setDescription('Command to import departments');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $numberDataSaved = 0;
        $numberDataUpdated = 0;

        $io->title('Importing departments...');

        $reader = Reader::createFromPath('src/Data/departments.csv', 'r');
        $reader->setHeaderOffset(0);

        $results = $reader->getRecords([
            'code',
            'name'
        ]);

        $resultsNumber = iterator_count($results);

        $departmentRepository = $this->em->getRepository(Department::class);

        $io->progressStart($resultsNumber);

        foreach ($results as $row) {

            if (strlen($row['code']) < 2 ) {
                $row['code'] = '0' . $row['code'];
            }

            $update = false;
            $department = $departmentRepository->findOneBy([
                'departmentCode' => $row['code']
            ]);

            if (!$department) {
                $department = new Department();
                $numberDataSaved++;
            }

            else {
                $update = true;
                $numberDataUpdated++;
            }

            $department->setDepartmentCode($row['code']);
            $department->setName($row['name']);

            if (!$update) {
                $this->em->persist($department);
            }

            $io->progressAdvance();
        }
        $this->em->flush();


        $io->progressFinish();

        $output->writeln($numberDataSaved. ' / ' . $resultsNumber . ' departments has been saved');
        $output->writeln($numberDataUpdated. ' / ' . $resultsNumber . ' departments has been updated');
        $io->success("Departments imported successfully !");

        return Command::SUCCESS;
    }
}
