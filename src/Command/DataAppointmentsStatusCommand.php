<?php

namespace App\Command;

use App\Entity\AppointmentStatus;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DataAppointmentsStatusCommand extends Command
{
    protected static $defaultName = 'data:appointments-status';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this->setDescription('Command to set appointments status');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->title('Setting appointments status...');

        $appointmentStatusRepository = $this->em->getRepository(AppointmentStatus::class);

        foreach(AppointmentStatus::APPOINTMENTS_STATUS as $appointmentStatusName) {
            $appointmentStatus = $appointmentStatusRepository->findOneBy([
                'name' => $appointmentStatusName
            ]);

            if ($appointmentStatus) {
                continue;
            }

            else {
                $appointmentStatus = new AppointmentStatus();
                $appointmentStatus->setName($appointmentStatusName);
                $this->em->persist($appointmentStatus);
            }
        }

        $this->em->flush();

        $io->success("Appointments status set successfully !");

        return Command::SUCCESS;
    }
}
