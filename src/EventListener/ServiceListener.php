<?php

namespace App\EventListener;

use App\Entity\Service;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ServiceListener
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function prePersist(Service $service, LifecycleEventArgs $event)
    {
        if ($this->tokenStorage->getToken()) {
            $user = $this->tokenStorage->getToken()->getUser();
            $service->setCreatedBy($user);
        }
    }

    public function preUpdate(Service $service, LifecycleEventArgs $event)
    {
        if ($this->tokenStorage->getToken()) {
            $user = $this->tokenStorage->getToken()->getUser();
            $service->setUpdatedBy($user);
        }
    }
}