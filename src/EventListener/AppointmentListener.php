<?php

namespace App\EventListener;

use App\Entity\Appointment;
use App\Entity\AppointmentStatus;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AppointmentListener
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(TokenStorageInterface $tokenStorage, EntityManagerInterface $entityManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->em = $entityManager;
    }

    public function prePersist(Appointment $appointment, LifecycleEventArgs $event)
    {
        if ($this->tokenStorage->getToken()) {
            $user = $this->tokenStorage->getToken()->getUser();
            $appointment->setClient($user);

            $appointmentStatus = $this->em->getRepository(AppointmentStatus::class)->findOneBy([
                'name' => 'Prévu'
            ]);

            $appointment->setAppointmentStatus($appointmentStatus);
        }
    }

}