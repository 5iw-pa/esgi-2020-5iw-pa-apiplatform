<?php

namespace App\Security\Voter;

use App\Entity\Establishment;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class EstablishmentVoter extends Voter
{
    const READ_AS_MANAGER_RULE = 'READ_ESTABLISHMENT_AS_MANAGER';
    const EDIT_AS_MANAGER_RULE = 'EDIT_ESTABLISHMENT_AS_MANAGER';
    const EDIT_AS_OWNER_RULE = 'EDIT_ESTABLISHMENT_AS_OWNER';
    const EDIT_AS_ADMIN_RULE = 'EDIT_ESTABLISHMENT_AS_ADMIN';
    const DELETE_RULE = 'DELETE_ESTABLISHMENT';

    /**
     * @var Security
     */
    private $security;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(Security $security, EntityManagerInterface $em)
    {
        $this->security = $security;
        $this->em = $em;
    }

    protected function supports($attribute, $subject): bool
    {
        return in_array($attribute, [self::READ_AS_MANAGER_RULE, self::EDIT_AS_MANAGER_RULE, self::EDIT_AS_OWNER_RULE, self::EDIT_AS_ADMIN_RULE, self::DELETE_RULE])
            && $subject instanceof Establishment;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $currentUser = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$currentUser instanceof UserInterface) {
            return false;
        }

        // always grant access if user is admin
        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        $establishment = $subject;

        switch ($attribute) {

            case self::READ_AS_MANAGER_RULE:
                return $this->canReadAsManager($currentUser, $establishment);

            case self::EDIT_AS_MANAGER_RULE:
                return $this->canEditAsManager($currentUser, $establishment);

            case self::EDIT_AS_OWNER_RULE:
                return $this->canEditAsOwner($currentUser, $establishment);

            case self::EDIT_AS_ADMIN_RULE:
                return $this->canEditAsAdmin($currentUser, $establishment);

            case self::DELETE_RULE:
                return $this->canDelete($currentUser, $establishment);

        }

        return false;
    }

    private function canReadAsManager (User $currentUser, $establishment): bool
    {
        // only manager and owner may have access
        if ( in_array('ROLE_MANAGER', $currentUser->getRoles())) {

            if ($establishment->getOwner() === $currentUser) {
                return true;
            }

            foreach ($establishment->getManagers() as $manager) {
                if ($currentUser === $manager) {
                    return true;
                }
            }

        }

        return false;
    }

    private function canEditAsManager (User $currentUser, $establishment): bool
    {

        // only manager and owner may have access
        if ( in_array('ROLE_MANAGER', $currentUser->getRoles())) {

            if ($establishment->getOwner() === $currentUser) {
                return true;
            }

            foreach ($establishment->getManagers() as $manager) {
                if ($currentUser === $manager) {
                    return true;
                }
            }

        }

        return false;
    }

    private function canEditAsOwner (User $currentUser, $establishment): bool
    {
        // only manager may have access
        if ( in_array('ROLE_MANAGER', $currentUser->getRoles())) {
            if ($establishment->getOwner() === $currentUser) {
                return true;
            }
        }

        return false;
    }

    // this function should never be reached, as admin role grant access to all
    private function canEditAsAdmin (User $currentUser, $establishment): bool
    {

        return false;
    }

    private function canDelete (User $currentUser, $establishment): bool
    {
        if ( in_array('ROLE_MANAGER', $currentUser->getRoles())) {
            if ($establishment->getOwner() === $currentUser) {
                return true;
            }
        }

        return false;
    }
}
