<?php

namespace App\Security\Voter;

use App\Entity\User;
use App\Entity\UserEstablishment;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class UserVoter extends Voter
{
    const READ_RULE = 'READ_USER';
    const LIST_RULE = 'LIST_USER';
    const CREATE_RULE = 'CREATE_USER';
    const EDIT_RULE = 'EDIT_USER';
    const DELETE_RULE = 'DELETE_USER';

    /**
     * @var Security
     */
    private $security;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(Security $security, EntityManagerInterface $em)
    {
        $this->security = $security;
        $this->em = $em;
    }

    protected function supports($attribute, $subject): bool
    {
        return in_array($attribute, [self::LIST_RULE, self::READ_RULE, self::CREATE_RULE, self::EDIT_RULE, self::DELETE_RULE])
            && $subject instanceof User;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $currentUser = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$currentUser instanceof UserInterface) {
            return false;
        }

        // always grant access if user is admin
        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        $userInstanceResource = $subject;

        // grant access if user is targeting himself as resource
        // example: edit his account
        if ($currentUser === $userInstanceResource) {
            return true;
        }

        switch ($attribute) {

            case self::LIST_RULE:
                return $this->canList($currentUser, $userInstanceResource);

            case self::READ_RULE:
                return $this->canRead($currentUser, $userInstanceResource);

            case self::CREATE_RULE:
                return $this->canCreate($currentUser, $userInstanceResource);

            case self::EDIT_RULE:
                return $this->canEdit($currentUser, $userInstanceResource);

            case self::DELETE_RULE:
                return $this->canDelete($currentUser, $userInstanceResource);

        }

        return false;
    }

    private function canList (User $authUser, $userInstanceResource): bool
    {

        return false;
    }

    private function canRead (User $currentUser, $userInstanceResource): bool
    {
        // only manager may have access
        if ( in_array('ROLE_MANAGER', $currentUser->getRoles())) {

            // case when manager try to access another manager resource
            // grant only if they have at least 1 common establishment
            if ( in_array('ROLE_MANAGER', $userInstanceResource->getRoles())) {

                $userEstablishmentRepository = $this->em->getRepository(UserEstablishment::class);

                // get all establishments managed by current manager
                $managerEstablishments = $userEstablishmentRepository->findBy([
                    'manager' => $currentUser
                ]);

                // get all establishments managed by targeted manager
                $userInstanceResourceEstablishments = $userEstablishmentRepository->findBy([
                    'manager' => $userInstanceResource
                ]);

                // compare to know if they have at least 1 common establishment
                foreach ($managerEstablishments as $managerEstablishment) {

                    $establishment = $managerEstablishment->getEstablishment();

                    foreach ($userInstanceResourceEstablishments as $userInstanceResourceEstablishment) {

                        if ($userInstanceResourceEstablishment->getEstablishment() === $establishment) {
                            return true;
                        }
                    }

                }
            }

            // case when manager try to access to a client resource
            // grant access only if client has already taken an appointment with an establishment managed by current manager
            else if ( in_array('ROLE_CLIENT', $userInstanceResource->getRoles())) {

                $userEstablishmentRepository = $this->em->getRepository(UserEstablishment::class);

                // get all establishments managed by current manager
                $managerEstablishments = $userEstablishmentRepository->findBy([
                    'manager' => $currentUser
                ]);

                // get client appointments to know if client has already taken appointment
                // in one of establishment managed by current user
                $clientAppointments = $userInstanceResource->getAppointments();

                foreach ($managerEstablishments as $managerEstablishment) {

                    $establishment = $managerEstablishment->getEstablishment();

                    foreach ($clientAppointments as $clientAppointment) {

                        if ($clientAppointment->getEstablishment() === $establishment) {
                            return true;
                        }
                    }
                }
            }

        }

        return false;
    }

    private function canCreate (User $authUser, $userInstanceResource): bool
    {

        return false;
    }

    private function canEdit (User $authUser, $userInstanceResource): bool
    {

        return false;
    }

    private function canDelete (User $authUser, $userInstanceResource): bool
    {

        return false;
    }
}
