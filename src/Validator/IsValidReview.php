<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsValidReview extends Constraint
{
    /*
     * Any public properties become valid options for the annotation.
     * Then, use these in your validator class.
     */
    public $alreadyReviewed = "Vous avez déjà attribué une note à l'établissement";

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
