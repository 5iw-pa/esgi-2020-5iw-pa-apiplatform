<?php

namespace App\Validator;

use App\Entity\Hours;
use App\Entity\TypeOfShift;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsValidHoursValidator extends ConstraintValidator
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $isHoursCreated = true;

        if ($value->getId()) {
            $isHoursCreated = false;
        }

        $typeOfShiftRepository = $this->em->getRepository(TypeOfShift::class);
        $hoursRepository = $this->em->getRepository(Hours::class);
        $morningShiftType = $typeOfShiftRepository->findOneBy(['name' => 'Matin']);
        $afternoonShiftType = $typeOfShiftRepository->findOneBy(['name' => 'Après-midi']);
        $fullDayShiftType = $typeOfShiftRepository->findOneBy(['name' => 'Journée entière']);

        $establishment = $value->getEstablishment();
        $establishmentHours = $establishment->getOpenHours();

        if ($value->getEndHour() <= $value->getStartHour()) {
            $this->context
                ->buildViolation($constraint->notCorrectHours)
                ->atPath('endHour')
                ->addViolation();

            return;
        }

        foreach ($establishmentHours as $hour) {

            // check if there are hours for current day
            if ($hour->getDay() === $value->getDay()) {

                // check if current day hours is a full day shift
                if ($hour->getTypeOfShift() && $value->getTypeOfShift() === $fullDayShiftType) {
                    $this->context
                        ->buildViolation($constraint->alreadyExistFullDayHours)
                        ->atPath('typeOfShift')
                        ->addViolation();
                    return;
                }

                else {

                    // handle morning condition
                    if ($morningShiftType === $value->getTypeOfShift()) {

                        if ($isHoursCreated && $hour->getTypeOfShift() === $morningShiftType) {
                            $this->context
                                ->buildViolation($constraint->alreadyExistMorningHours)
                                ->atPath('typeOfShift')
                                ->addViolation();
                            return;
                        }

                        $afternoonHours = $hoursRepository->getAfternoonHoursFromEstablishment($establishment, $value->getDay());

                        if ($afternoonHours && $afternoonHours->getStartHour() < $value->getEndHour()) {
                            $this->context
                                ->buildViolation($constraint->morningHoursTooLate)
                                ->atPath('endHour')
                                ->addViolation();
                            return;
                        }
                    }

                    // handle afternoon condition
                    if ($afternoonShiftType === $value->getTypeOfShift()) {

                        if ($isHoursCreated && $hour->getTypeOfShift() === $afternoonShiftType) {
                            $this->context
                                ->buildViolation($constraint->alreadyExistAfternoonHours)
                                ->atPath('typeOfShift')
                                ->addViolation();
                            return;
                        }

                        $morningHours = $hoursRepository->getMorningHoursFromEstablishment($establishment, $value->getDay());

                        if ($morningHours && $value->getStartHour() < $morningHours->getEndHour()) {
                            $this->context
                                ->buildViolation($constraint->afternoonHoursTooEarly)
                                ->atPath('startHour')
                                ->addViolation();
                            return;
                        }
                    }

                }
            }
        }

    }
}
