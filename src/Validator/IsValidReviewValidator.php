<?php

namespace App\Validator;

use App\Entity\Review;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsValidReviewValidator extends ConstraintValidator
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(EntityManagerInterface $entityManager, TokenStorageInterface $tokenStorage)
    {
        $this->em = $entityManager;
        $this->tokenStorage = $tokenStorage;
    }

    public function validate($value, Constraint $constraint)
    {
        $establishment = $value->getEstablishment();
        $client = $this->tokenStorage->getToken()->getUser();

        $review = $this->em->getRepository(Review::class)->findOneBy([
            'establishment' => $establishment,
            'author' => $client
        ]);

        if ($review) {
            $this->context
                ->buildViolation($constraint->alreadyReviewed)
                ->atPath('establishment')
                ->addViolation();
            return;
        }

    }
}
