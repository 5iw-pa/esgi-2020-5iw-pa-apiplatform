<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsValidAppointment extends Constraint
{
    public $dateMissing = "La date et l'heure ne sont pas valide";

    public $hourTooEarly = "La date et l'heure ne sont pas valide, 
    le rendez-vous doit être pris au plus tard 15 minutes avant l'heure de début";

    public $notCorrectServices = "Les prestations renseignées ne sont pas valides,
    veuillez sélectionner des prestations appartenant à l'établissement";

    public $establishmentMissing = "L'établissement est olbigatoire";

    public $establishmentClosed = "L'établissement est fermé à la date et heure indiqué pour le rendez-vous, 
    veuillez sélectionner une autre date ou une heure différente";

    public $notAvailableBarber = "Aucun coiffeur n'est disponible pour l'heure et la date de rendez-vous souhaité, 
    veuillez sélectionner une autre date ou une heure différente";

    public $establishmentCloseSoon = "La durée du rendez-vous dépasse l'horaire de fermeture de l'établissement,
    veuillez sélectionner une autre date ou une heure différente";

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
