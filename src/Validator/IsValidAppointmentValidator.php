<?php

namespace App\Validator;

use App\Entity\Appointment;
use App\Entity\Establishment;
use App\Entity\Hours;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsValidAppointmentValidator extends ConstraintValidator
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function validate($value, Constraint $constraint)
    {
        $isAppointmentCreated = true;

        if ($value->getId()) {
            $isAppointmentCreated = false;
        }

        $establishment = $value->getEstablishment();
        $appointmentDate = $value->getStartHour();

        if (!$appointmentDate) {
            $this->context
                ->buildViolation($constraint->dateMissing)
                ->atPath('startHour')
                ->addViolation();
            return;
        }

        if (($appointmentDate <= new DateTime('now +15 minutes')) && $isAppointmentCreated) {
            $this->context
                ->buildViolation($constraint->hourTooEarly)
                ->atPath('startHour')
                ->addViolation();
            return;
        }

        if (!$establishment) {
            $this->context
                ->buildViolation($constraint->establishmentMissing)
                ->atPath('establishment')
                ->addViolation();
            return;
        }

        $relatedHour = $this->isEstablishmentOpen($establishment, $appointmentDate);

        if (!$relatedHour && $isAppointmentCreated) {
            $this->context
                ->buildViolation($constraint->establishmentClosed)
                ->atPath('startHour')
                ->addViolation();
            return;
        }

        $isBarberAvailable = $this->isBarberAvailable($establishment, $value, $relatedHour);

        if (!$isBarberAvailable && $isAppointmentCreated) {
            $this->context
                ->buildViolation($constraint->notAvailableBarber)
                ->atPath('startHour')
                ->addViolation();
            return;
        }

        $isServicesOk = $this->doServicesBelongToEstablishments($establishment, $value->getServices());

        if (!$isServicesOk && $isAppointmentCreated) {
            $this->context
                ->buildViolation($constraint->notCorrectServices)
                ->atPath('services')
                ->addViolation();
            return;
        }

        if (($value->getEndHour()->format('H:i') > $relatedHour->getEndHour()) && $isAppointmentCreated) {
            $this->context
                ->buildViolation($constraint->establishmentCloseSoon)
                ->atPath('startHour')
                ->addViolation();
            return;
        }
    }

    /**
     * @param Establishment $establishment
     * @param DateTime $appointmentDate
     * @return Hours|null
     */
    private function isEstablishmentOpen (Establishment $establishment, DateTime $appointmentDate): ?Hours
    {
        return $establishment->getRelatedHour($appointmentDate);
    }

    /**
     * @param Establishment $establishment
     * @param Appointment $appointment
     * @param Hours $relatedHour
     * @return bool
     * @throws Exception
     */
    private function isBarberAvailable (Establishment $establishment, Appointment $appointment, Hours $relatedHour): bool
    {
        $isBarberAvailable = true;

        $appointmentRepository = $this->em->getRepository(Appointment::class);
        $appointments = $appointmentRepository->getAppointmentsAtDate($establishment, $appointment->getStartHour());

        $occupiedBarber = 0;

        foreach ($appointments as $currentAppointment) {
            $isCurrentAppointmentBeforeOrOver = $currentAppointment->getStartHour() <= $appointment->getStartHour() && $currentAppointment->getEndHour() > $appointment->getStartHour();
            $isCurrentAppointmentAfterOrOver = $currentAppointment->getStartHour() >= $appointment->getStartHour() && $currentAppointment->getStartHour() < $appointment->getEndHour();

            $isOneBarberOccupied = $isCurrentAppointmentBeforeOrOver || $isCurrentAppointmentAfterOrOver;
            if ($isOneBarberOccupied) {
                $occupiedBarber++;
            }
        }

        if ($occupiedBarber + 1 > $relatedHour->getNbBarber()) {
            $isBarberAvailable = false;
        }

        return $isBarberAvailable;
    }

    /**
     * @param Establishment $establishment
     * @param $services
     * @return bool|null
     */
    private function doServicesBelongToEstablishments (Establishment $establishment, $services): ?bool
    {
        $isServicesOk = true;

        foreach ($services as $service) {
            if (!$establishment->doesServiceBelongTo($service)) {
                $isServicesOk = false;
                break;
            }
        }

        return $isServicesOk;
    }
}
