<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsValidHours extends Constraint
{
    public $notCorrectHours = "Les horaires indiqués ne sont pas corrects, l'heure de fin ne peut pas être antérieur à l'heure de début";
    public $alreadyExistFullDayHours = 'Il existe déjà des horaires sur le créneau souhaité (journée entière)';
    public $alreadyExistMorningHours = 'Il existe déjà des horaires sur le créneau souhaité (matin)';
    public $alreadyExistAfternoonHours = 'Il existe déjà des horaires sur le créneau souhaité (après-midi)';
    public $morningHoursTooLate = "Les horaires du matin ne peuvent se terminer après que celles de l'après-midi aient débuté";
    public $afternoonHoursTooEarly = "Les horaires de l'après-midi ne peuvent débuter avant que celles du matin ne soient fini";

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
