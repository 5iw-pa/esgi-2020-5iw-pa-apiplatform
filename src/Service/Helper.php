<?php

namespace App\Service;

use DateTime;

class Helper
{
    /**
     * @param String $englishName
     * @return string
     */
    static function getFrenchDayName(String $englishName): ?String
    {
        $days = [
            'Monday' => 'Lundi',
            'Tuesday' => 'Mardi',
            'Wednesday' => 'Mercredi',
            'Thursday' => 'Jeudi',
            'Friday' => 'Vendredi',
            'Saturday' => 'Samedi',
            'Sunday' => 'Dimanche'
        ];

        return $englishName ? $days[$englishName] : null;
    }

    /**
     * @param $date
     * @param string $format
     * @return bool
     */
    static function validateDate($date, $format = 'd-m-Y'): bool
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

}
