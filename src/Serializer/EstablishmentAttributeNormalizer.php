<?php

namespace App\Serializer;

use App\Entity\Establishment;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;

class EstablishmentAttributeNormalizer implements ContextAwareNormalizerInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;

    private const ALREADY_CALLED = 'ESTABLISHMENT_ATTRIBUTE_NORMALIZER_ALREADY_CALLED';

    private $tokenStorage;
    private $authorizationChecker;


    public function __construct(TokenStorageInterface $tokenStorage, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        $operationName = $context['item_operation_name'];

        if ($this->userHasPermissionsForEstablishment($object, $operationName)) {
            $context['groups'][] = 'establishment:manager:read';
        }

        $context[self::ALREADY_CALLED] = true;

        return $this->normalizer->normalize($object, $format, $context);
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        // Make sure we're not calling twice
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        // only support operation of type item
        if (isset($context['operation_type']) && $context['operation_type'] !== "item") {
            return false;
        }

        return $data instanceof Establishment;
    }

    private function userHasPermissionsForEstablishment($object, $operationName = ''): bool
    {

        if ($operationName === 'get') {
            return $this->authorizationChecker->isGranted('READ_ESTABLISHMENT_AS_MANAGER', $object);
        }

        return false;
    }
}