<?php

namespace App\Serializer;

use ApiPlatform\Core\Serializer\SerializerContextBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use App\Entity\Establishment;

final class EstablishmentContextBuilder implements SerializerContextBuilderInterface
{
    private $decorated;
    private $authorizationChecker;

    public function __construct(SerializerContextBuilderInterface $decorated, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->decorated = $decorated;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function createFromRequest(Request $request, bool $normalization, ?array $extractedAttributes = null): array
    {
        $context = $this->decorated->createFromRequest($request, $normalization, $extractedAttributes);
        $resourceClass = $context['resource_class'] ?? null;
        $isItemOperation = $context['operation_type'] === 'item';

        if ($resourceClass === Establishment::class && isset($context['groups']) && true === $normalization) {

            // if user is admin and operation is a collection operation, allow him to get more data from get collection operation
            if ($this->authorizationChecker->isGranted('ROLE_ADMIN') && !$isItemOperation) {
                $context['groups'][] = 'establishment:admin:list';
            }
        }

        return $context;
    }
}