<?php

namespace App\Serializer;

use App\Entity\Establishment;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;


class EstablishmentAttributeDenormalizer implements ContextAwareDenormalizerInterface, DenormalizerAwareInterface
{
    use DenormalizerAwareTrait;

    private const ALREADY_CALLED = 'ESTABLISHMENT_ATTRIBUTE_DENORMALIZER_ALREADY_CALLED';

    private $tokenStorage;
    private $authorizationChecker;


    public function __construct(TokenStorageInterface $tokenStorage, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function denormalize($data, $type, $format = null, array $context = [])
    {
        $operationName = $context['item_operation_name'];

        $object = $context['object_to_populate'];

        if ($this->userHasPermissionsForEstablishment($object, $operationName)) {

            if ($this->authorizationChecker->isGranted('EDIT_ESTABLISHMENT_AS_OWNER', $object)) {
                $context['groups'][] = 'establishment:owner:write';
            }

            if ($this->authorizationChecker->isGranted('EDIT_ESTABLISHMENT_AS_ADMIN', $object)) {
                $context['groups'][] = 'establishment:admin:write';
            }

        }

        $context[self::ALREADY_CALLED] = true;

        return $this->denormalizer->denormalize($data, $type, $format, $context);
    }

    public function supportsDenormalization($data, $type, $format = null, array $context = []): bool
    {
        // Make sure we're not calling twice
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        // only support operation of type item
        if (isset($context['operation_type']) && $context['operation_type'] !== "item") {
            return false;
        }

        return Establishment::class === $type;
    }

    private function userHasPermissionsForEstablishment($object, $operationName = ''): bool
    {
        if ($operationName === 'patch') {
            return $this->authorizationChecker->isGranted('EDIT_ESTABLISHMENT_AS_MANAGER', $object);
        }

        return false;
    }
}