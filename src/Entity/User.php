<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserRepository;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\ActiveTrait;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Entity\Traits\DeletedTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\User\RegisterClientController;
use App\Controller\User\RegisterManagerController;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ApiResource(
 *     collectionOperations={
 *         "register_client"={
 *              "method"="POST",
 *              "path"="/users/client/register",
 *              "controller"=RegisterClientController::class,
 *              "denormalization_context"={"groups"={"user:write"}},
 *              "normalization_context"={"groups"={"user:read"}}
 *          },
 *          "register_manager"={
 *              "method"="POST",
 *              "path"="/users/manager/register",
 *              "controller"=RegisterManagerController::class,
 *              "denormalization_context"={"groups"={"user:write"}},
 *              "normalization_context"={"groups"={"user:read"}}
 *          },
 *          "get"={
 *             "normalization_context"={"groups"={"user:read"}},
 *             "security"="is_granted('ROLE_ADMIN')"
 *         }
 *     },
 *     itemOperations={
 *         "get"={
 *             "normalization_context"={"groups"={"user:read"}},
 *             "security"="is_granted('READ_USER', object)",
 *         },
 *         "patch"={
 *             "denormalization_context"={"groups"={"user:write"}},
 *             "normalization_context"={"groups"={"user:read"}},
 *             "security"="is_granted('EDIT_USER', object)"
 *         },
 *         "delete"={
 *             "security"="is_granted('DELETE_USER', object)"
 *         },
 *     }
 * )
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "email": "partial"})
 * @UniqueEntity(fields={"email"}, message="Cet e-mail est déjà enregistré")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 */
class User implements UserInterface
{
    use ActiveTrait;
    use DeletedTrait;
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"user:read", "appointement:read", "report:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"user:write", "user:read", "establishment:manager:read", "userEstablishment:owner:read", "report:read"})
     * @Assert\Email(message="Cet e-mail n'est pas valide")
     * @Assert\NotBlank(message="L'e-mail est obligatoire")
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Groups({"user:write"})
     * @Assert\NotBlank(message="Le mot de passe est obligatoire")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({
     *     "user:write",
     *     "user:read",
     *     "establishment:manager:read",
     *     "userEstablishment:owner:read",
     *     "appointement:read",
     *     "review:read",
     *     "report:read"
     * })
     * @Assert\NotBlank(message="Le prénom est obligatoire")
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({
     *     "user:write",
     *     "user:read",
     *     "establishment:manager:read",
     *     "userEstablishment:owner:read",
     *     "appointement:read",
     *     "review:read",
     *     "report:read"
     * })
     * @Assert\NotBlank(message="Le nom est obligatoire")
     */
    private $lastname;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"user:write", "user:read", "userEstablishment:owner:read"})
     * @Assert\NotBlank(message="La date de naissance est obligatoire")
     * @Assert\Type(type="datetime", message="Cette date est invalide")
     */
    private $birthdate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user:write", "user:read", "userEstablishment:owner:read", "appointement:read"})
     * @Assert\NotBlank(message="Le sexe est obligatoire")
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({
     *     "user:write",
     *     "user:read",
     *     "establishment:manager:read",
     *     "userEstablishment:owner:read",
     *     "appointement:read",
     *     "report:read"
     * })
     * @Assert\NotBlank(message="Le numéro de téléphone est obligatoire")
     */
    private $phoneNumber;

    /**
     * @ORM\ManyToOne(targetEntity=Department::class, inversedBy="users")
     * @Groups({"user:write", "user:read", "userEstablishment:owner:read", "appointement:read"})
     * @Assert\NotBlank(message="Le département est obligatoire")
     */
    private $department;

    /**
     * @ORM\OneToMany(targetEntity=Report::class, mappedBy="author")
     */
    private $reports;

    /**
     * @ORM\OneToMany(targetEntity=Review::class, mappedBy="author")
     */
    private $reviews;

    /**
     * @ORM\OneToMany(targetEntity=Establishment::class, mappedBy="owner")
     */
    private $ownedEstablishments;

    /**
     * @ORM\OneToMany(targetEntity=UserEstablishment::class, mappedBy="manager")
     */
    private $userEstablishments;

    /**
     * @ORM\OneToMany(targetEntity=Appointment::class, mappedBy="client")
     */
    private $appointments;

    public function __construct()
    {
        $this->reports = new ArrayCollection();
        $this->reviews = new ArrayCollection();
        $this->ownedEstablishments = new ArrayCollection();
        $this->userEstablishments = new ArrayCollection();
        $this->appointments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getBirthdate(): ?\DateTimeInterface
    {
        return $this->birthdate;
    }

    public function setBirthdate(?\DateTimeInterface $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(?Department $department): self
    {
        $this->department = $department;

        return $this;
    }

    /**
     * @return Collection|Report[]
     */
    public function getReports(): Collection
    {
        return $this->reports;
    }

    public function addReport(Report $report): self
    {
        if (!$this->reports->contains($report)) {
            $this->reports[] = $report;
            $report->setAuthor($this);
        }

        return $this;
    }

    public function removeReport(Report $report): self
    {
        if ($this->reports->removeElement($report)) {
            // set the owning side to null (unless already changed)
            if ($report->getAuthor() === $this) {
                $report->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Review[]
     */
    public function getReviews(): Collection
    {
        return $this->reviews;
    }

    public function addReview(Review $review): self
    {
        if (!$this->reviews->contains($review)) {
            $this->reviews[] = $review;
            $review->setAuthor($this);
        }

        return $this;
    }

    public function removeReview(Review $review): self
    {
        if ($this->reviews->removeElement($review)) {
            // set the owning side to null (unless already changed)
            if ($review->getAuthor() === $this) {
                $review->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Establishment[]
     */
    public function getOwnedEstablishments(): Collection
    {
        return $this->ownedEstablishments;
    }

    public function addOwnedEstablishment(Establishment $ownedEstablishment): self
    {
        if (!$this->ownedEstablishments->contains($ownedEstablishment)) {
            $this->ownedEstablishments[] = $ownedEstablishment;
            $ownedEstablishment->setOwner($this);
        }

        return $this;
    }

    public function removeOwnedEstablishment(Establishment $ownedEstablishment): self
    {
        if ($this->ownedEstablishments->removeElement($ownedEstablishment)) {
            // set the owning side to null (unless already changed)
            if ($ownedEstablishment->getOwner() === $this) {
                $ownedEstablishment->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserEstablishment[]
     */
    public function getUserEstablishments(): Collection
    {
        return $this->userEstablishments;
    }

    public function addUserEstablishment(UserEstablishment $userEstablishment): self
    {
        if (!$this->userEstablishments->contains($userEstablishment)) {
            $this->userEstablishments[] = $userEstablishment;
            $userEstablishment->setManager($this);
        }

        return $this;
    }

    public function removeUserEstablishment(UserEstablishment $userEstablishment): self
    {
        if ($this->userEstablishments->removeElement($userEstablishment)) {
            // set the owning side to null (unless already changed)
            if ($userEstablishment->getManager() === $this) {
                $userEstablishment->setManager(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Appointment[]
     */
    public function getAppointments(): Collection
    {
        return $this->appointments;
    }

    public function addAppointment(Appointment $appointment): self
    {
        if (!$this->appointments->contains($appointment)) {
            $this->appointments[] = $appointment;
            $appointment->setClient($this);
        }

        return $this;
    }

    public function removeAppointment(Appointment $appointment): self
    {
        if ($this->appointments->removeElement($appointment)) {
            // set the owning side to null (unless already changed)
            if ($appointment->getClient() === $this) {
                $appointment->setClient(null);
            }
        }

        return $this;
    }

    /**
     * @return Establishment[]
     */
    public function getMyEstablishmentsAsManager(): array
    {
        $establishments = [];

        if (in_array('ROLE_MANAGER', $this->roles)) {
            foreach ($this->userEstablishments as $userEstablishment) {
                if ($userEstablishment->getValidated()) {
                    $establishments[] = $userEstablishment->getEstablishment();
                }
            }
        }


        return $establishments;
    }
}
