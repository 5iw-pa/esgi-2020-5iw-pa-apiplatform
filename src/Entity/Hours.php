<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\ActiveTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Repository\HoursRepository;
use App\Validator\IsValidHours;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "post"={
 *             "denormalization_context"={"groups"={"hours:write"}},
 *             "normalization_context"={"groups"={"hours:read"}},
 *             "security_post_denormalize"="is_granted('EDIT_ESTABLISHMENT_AS_OWNER', object.getEstablishment())"
 *         }
 *     },
 *     itemOperations={
 *         "get"={
 *             "normalization_context"={"groups"={"hours:read"}}
 *         },
 *         "patch"={
 *             "denormalization_context"={"groups"={"hours:write"}},
 *             "normalization_context"={"groups"={"hours:read"}},
 *             "security"="is_granted('EDIT_ESTABLISHMENT_AS_OWNER', object.getEstablishment())"
 *         },
 *         "delete"={
 *             "security"="is_granted('EDIT_ESTABLISHMENT_AS_OWNER', object.getEstablishment())"
 *         }
 *     }
 * )
 * @ORM\Entity(repositoryClass=HoursRepository::class)
 * @IsValidHours()
 */
class Hours
{
    use ActiveTrait;
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"hours:read", "establishment:default:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="time", nullable=true)
     * @Groups({"hours:read", "hours:write", "establishment:default:read"})
     */
    private $startHour;

    /**
     * @ORM\Column(type="time", nullable=true)
     * @Groups({"hours:read", "hours:write", "establishment:default:read"})
     */
    private $endHour;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"hours:read", "hours:write", "establishment:default:read"})
     * @Assert\PositiveOrZero(message="Le nombre de coiffeur disponible ne peut être inférieur à 0")
     */
    private $nbBarber;

    /**
     * @ORM\ManyToOne(targetEntity=Establishment::class, inversedBy="openHours")
     * @Groups({"hours:read", "hours:write"})
     * @Assert\NotBlank(message="L'établissement est obligatoire")
     */
    private $establishment;

    /**
     * @ORM\ManyToOne(targetEntity=Day::class, inversedBy="hours")
     * @Groups({"hours:read", "hours:write", "establishment:default:read"})
     * @Assert\NotBlank(message="Le jour est obligatoire")
     */
    private $day;

    /**
     * @ORM\ManyToOne(targetEntity=TypeOfShift::class, inversedBy="hours")
     * @Groups({"hours:read", "hours:write", "establishment:default:read"})
     * @Assert\NotBlank(message="Le type de plage horaires est obligatoire")
     */
    private $typeOfShift;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartHour(): ?String
    {
        return $this->startHour->format('H:i');
    }

    public function setStartHour(?\DateTimeInterface $startHour): self
    {
        $this->startHour = $startHour;

        return $this;
    }

    public function getEndHour(): ?String
    {
        return $this->endHour->format('H:i');
    }

    public function setEndHour(?\DateTimeInterface $endHour): self
    {
        $this->endHour = $endHour;

        return $this;
    }

    public function getNbBarber(): ?int
    {
        return $this->nbBarber;
    }

    public function setNbBarber(?int $nbBarber): self
    {
        $this->nbBarber = $nbBarber;

        return $this;
    }

    public function getEstablishment(): ?Establishment
    {
        return $this->establishment;
    }

    public function setEstablishment(?Establishment $establishment): self
    {
        $this->establishment = $establishment;

        return $this;
    }

    public function getDay(): ?Day
    {
        return $this->day;
    }

    public function setDay(?Day $day): self
    {
        $this->day = $day;

        return $this;
    }

    public function getTypeOfShift(): ?TypeOfShift
    {
        return $this->typeOfShift;
    }

    public function setTypeOfShift(?TypeOfShift $typeOfShift): self
    {
        $this->typeOfShift = $typeOfShift;

        return $this;
    }
}
