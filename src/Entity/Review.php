<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\DeletedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Repository\ReviewRepository;
use App\Validator\IsValidReview;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={
 *             "normalization_context"={"groups"={"review:read"}}
 *         },
 *         "post"={
 *             "denormalization_context"={"groups"={"review:write"}},
 *             "normalization_context"={"groups"={"review:read"}},
 *             "security"="is_granted('ROLE_CLIENT')"
 *         }
 *     },
 *     itemOperations={
 *         "get"={
 *             "normalization_context"={"groups"={"service:read"}}
 *         }
 *     }
 * )
 * @ORM\Entity(repositoryClass=ReviewRepository::class)
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @ApiFilter(SearchFilter::class, properties={"establishment": "exact"})
 * @IsValidReview()
 */
class Review
{
    use DeletedTrait;
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"review:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"review:write", "review:read"})
     * @Assert\NotBlank(message="La note de l'établissement est obligatoire")
     * @Assert\Range(
     *      min = 0,
     *      max = 5,
     *      notInRangeMessage = "Vous devez saisir une note ntre {{ min }} and {{ max }}"
     * )
     * @Assert\Type(type="integer", message="Veuillez saisir un nombre entier")
     */
    private $mark;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"review:write", "review:read"})
     * @Assert\NotBlank(message="Le contenu de l'avis est obligatoire")
     * @Assert\Type(type="string", message="Veuillez saisir un texte correct")
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="reviews")
     * @Groups({"review:read"})
     */
    private $author;

    /**
     * @ORM\ManyToOne(targetEntity=Establishment::class, inversedBy="reviews")
     * @Groups({"review:write", "review:read"})
     * @Assert\NotBlank(message="L'établissement est obligatoire")
     */
    private $establishment;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMark(): ?int
    {
        return $this->mark;
    }

    public function setMark(?int $mark): self
    {
        $this->mark = $mark;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getEstablishment(): ?Establishment
    {
        return $this->establishment;
    }

    public function setEstablishment(?Establishment $establishment): self
    {
        $this->establishment = $establishment;

        return $this;
    }
}
