<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\ActiveTrait;
use App\Entity\Traits\CreatedByTrait;
use App\Entity\Traits\DeletedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Repository\ServiceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "post"={
 *             "denormalization_context"={"groups"={"service:write"}},
 *             "normalization_context"={"groups"={"service:read"}},
 *             "security_post_denormalize"="is_granted('EDIT_ESTABLISHMENT_AS_MANAGER', object.getEstablishment())"
 *         }
 *     },
 *     itemOperations={
 *         "patch"={
 *             "denormalization_context"={"groups"={"service:write"}},
 *             "normalization_context"={"groups"={"service:read"}},
 *             "security"="is_granted('EDIT_ESTABLISHMENT_AS_MANAGER', object.getEstablishment())"
 *         },
 *         "get"={
 *             "normalization_context"={"groups"={"service:read"}}
 *         },
 *         "delete"={
 *             "security"="is_granted('EDIT_ESTABLISHMENT_AS_MANAGER', object.getEstablishment())"
 *         }
 *     }
 * )
 * @ORM\Entity(repositoryClass=ServiceRepository::class)
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 */
class Service
{
    use ActiveTrait;
    use DeletedTrait;
    use TimestampableTrait;
    use CreatedByTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"service:read", "establishment:default:read", "appointement:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"service:read","service:write", "establishment:default:read", "appointement:read"})
     * @Assert\NotBlank(message="Le nom de la prestation est obligatoire")
     */
    private $name;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"service:read","service:write", "establishment:default:read", "appointement:read"})
     * @Assert\NotBlank(message="Le prix de la prestation est obligatoire")
     * @Assert\Positive(message="Le prix de la prestation n'est pas correcte, veuillez saisir un nombre valide")
     */
    private $price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"service:read","service:write", "establishment:default:read", "appointement:read"})
     * @Assert\NotBlank(message="La durée de la prestation est obligatoire")
     * @Assert\Positive(message="La durée de la prestation n'est pas correcte, veuillez saisir une durée valide")
     * @Assert\Type(type="integer", message="La durée de la prestation n'est pas correcte, veuillez saisir une durée valide")
     */
    private $duration;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $illustration;

    /**
     * @ORM\ManyToOne(targetEntity=Establishment::class, inversedBy="services")
     * @Groups({"service:read","service:write"})
     */
    private $establishment;

    /**
     * @ORM\ManyToMany(targetEntity=Appointment::class, mappedBy="services")
     */
    private $appointments;

    public function __construct()
    {
        $this->appointments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getIllustration(): ?string
    {
        return $this->illustration;
    }

    public function setIllustration(?string $illustration): self
    {
        $this->illustration = $illustration;

        return $this;
    }

    public function getEstablishment(): ?Establishment
    {
        return $this->establishment;
    }

    public function setEstablishment(?Establishment $establishment): self
    {
        $this->establishment = $establishment;

        return $this;
    }

    /**
     * @return Collection|Appointment[]
     */
    public function getAppointments(): Collection
    {
        return $this->appointments;
    }

    public function addAppointment(Appointment $appointment): self
    {
        if (!$this->appointments->contains($appointment)) {
            $this->appointments[] = $appointment;
            $appointment->addService($this);
        }

        return $this;
    }

    public function removeAppointment(Appointment $appointment): self
    {
        if ($this->appointments->removeElement($appointment)) {
            $appointment->removeService($this);
        }

        return $this;
    }

    /**
     * @return bool
     * @Groups({"service:read", "service:write"})
     */
    public function isActive(): bool
    {
        return $this->active;
    }
}
