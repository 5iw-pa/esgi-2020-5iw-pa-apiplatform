<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\ActiveTrait;
use App\Entity\Traits\DeletedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Repository\EstablishmentRepository;
use App\Service\Helper;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\Establishment\GetMyEstablishmentsControllerAsManager;
use App\Controller\Establishment\GetMyEstablishmentsControllerAsOwner;
use App\Controller\Establishment\GetEstablishmentsInactiveController;
use App\Controller\Establishment\CreateMyEstablishmentController;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={
 *             "normalization_context"={"groups"={"establishment:default:list"}}
 *         },
 *         "get_my_establishments_as_manager"={
 *              "method"="GET",
 *              "path"="/establishments/manager/get-my-establishments",
 *              "controller"=GetMyEstablishmentsControllerAsManager::class,
 *              "normalization_context"={"groups"={"establishment:default:list"}},
 *              "security"="is_granted('ROLE_MANAGER')"
 *          },
 *         "get_my_establishments_as_owner"={
 *              "method"="GET",
 *              "path"="/establishments/owner/get-my-establishments",
 *              "controller"=GetMyEstablishmentsControllerAsOwner::class,
 *              "normalization_context"={"groups"={"establishment:default:list"}},
 *              "security"="is_granted('ROLE_MANAGER')"
 *          },
 *         "get_establishments_inactive"={
 *              "method"="GET",
 *              "path"="/establishments/get-establishments-inactive",
 *              "controller"=GetEstablishmentsInactiveController::class,
 *              "normalization_context"={"groups"={"establishment:default:list"}},
 *              "security"="is_granted('ROLE_ADMIN')"
 *          },
 *          "post"={
 *              "denormalization_context"={"groups"={
 *                  "establishment:manager:write",
 *                  "establishment:owner:write",
 *                  "establishment:admin:write"}
 *              },
 *              "normalization_context"={"groups"={"establishment:default:read"}},
 *              "security"="is_granted('ROLE_ADMIN')"
 *          },
 *          "create_my_establishment"={
 *              "method"="POST",
 *              "path"="/establishments/manager/create-my-establishment",
 *              "controller"=CreateMyEstablishmentController::class,
 *              "denormalization_context"={"groups"={"establishment:manager:write", "establishment:owner:write"}},
 *              "normalization_context"={"groups"={"establishment:default:read"}},
 *              "security"="is_granted('ROLE_MANAGER')"
 *          }
 *     },
 *     itemOperations={
 *         "get"={
 *             "normalization_context"={"groups"={"establishment:default:read"}}
 *         },
 *         "patch"={
 *             "denormalization_context"={"groups"={"establishment:manager:write"}},
 *             "normalization_context"={"groups"={"establishment:default:read", "establishment:manager:read"}},
 *             "security"="is_granted('EDIT_ESTABLISHMENT_AS_MANAGER', object)"
 *         },
 *         "delete"={
 *             "security"="is_granted('DELETE_ESTABLISHMENT', object)"
 *         }
 *     }
 * )
 * @ORM\Entity(repositoryClass=EstablishmentRepository::class)
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @ApiFilter(SearchFilter::class, properties={
 *     "city": "ipartial", "zipCode": "ipartial", "name": "ipartial", "department": "exact"
 * })
 * @UniqueEntity(fields={"siret"}, message="Cet siret déjà enregistré")
 */
class Establishment
{
    use ActiveTrait;
    use DeletedTrait;
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({
     *     "establishment:default:list",
     *     "establishment:default:read",
     *     "service:read",
     *     "hours:read",
     *     "appointement:read",
     *     "review:read",
     *     "report:read"
     * })
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({
     *     "establishment:admin:list",
     *     "establishment:manager:read",
     *     "establishment:owner:write",
     *     "userEstablishment:owner:read"
     * })
     * @Assert\NotBlank(message="Le nom de l'établissement est obligatoire")
     */
    private $commercialName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({
     *     "establishment:default:list",
     *     "establishment:default:read",
     *     "establishment:manager:write",
     *     "userEstablishment:owner:read",
     *     "service:read",
     *     "hours:read",
     *     "appointement:read",
     *     "review:read",
     *     "report:read"
     * })
     * @Assert\NotBlank(message="L'adresse de l'établissement est obligatoire")
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({
     *     "establishment:default:list",
     *     "establishment:default:read",
     *     "establishment:manager:write",
     *     "userEstablishment:owner:read",
     *     "service:read",
     *     "hours:read",
     *     "appointement:read",
     *     "review:read",
     *     "report:read"
     * })
     */
    private $addressComplement;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({
     *     "establishment:default:list",
     *     "establishment:default:read",
     *     "establishment:manager:write",
     *     "userEstablishment:owner:read",
     *     "service:read",
     *     "hours:read",
     *     "appointement:read",
     *     "review:read",
     *     "report:read"
     * })
     * @Assert\NotBlank(message="La ville de l'établissement est obligatoire")
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({
     *     "establishment:default:list",
     *     "establishment:default:read",
     *     "establishment:manager:write",
     *     "userEstablishment:owner:read",
     *     "service:read",
     *     "hours:read",
     *     "appointement:read",
     *     "review:read",
     *     "report:read"
     * })
     * @Assert\NotBlank(message="Le code postal l'établissement est obligatoire")
     * @Assert\Regex(
     *     pattern= "/^\d{5}$/",
     *     message= "Le code postal doit être composé de 5 chiffres"
     * )
     */
    private $zipCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({
     *     "establishment:admin:list",
     *     "establishment:manager:read",
     *     "establishment:owner:write",
     *     "userEstablishment:owner:read"
     * })
     * @Assert\NotBlank(message="Le siret de l'établissement est obligatoire")
     * @Assert\Regex(pattern="/^\d{14}/", message="Ce siret est invalide")
     */
    private $siret;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"establishment:admin:list", "establishment:default:read", "establishment:manager:write"})
     * @Assert\NotBlank(message="Le numéro l'établissement est obligatoire")
     * @Assert\Regex(
     *     pattern= "/^0[1-9]\d{8}$/",
     *     message="Le numéro de téléphone doit être composé de 10 chiffres"
     * )
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"establishment:default:read", "establishment:manager:write"})
     * @Assert\NotBlank(message="L'email de contact de l'établissement est obligatoire")
     * @Assert\Email(message="Cet e-mail n'est pas valide")
     */
    private $contactEmail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $illustration;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({
     *     "establishment:default:list",
     *     "establishment:default:read",
     *     "establishment:manager:write",
     *     "userEstablishment:owner:read",
     *     "service:read",
     *     "hours:read",
     *     "appointement:read",
     *     "review:read",
     *     "report:read"
     * })
     * @Assert\NotBlank(message="Le nom sous lequel vous souhaitez que votre établissement apparaisse est obligatoire")
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"establishment:default:list", "establishment:default:read", "establishment:manager:write"})
     * @Assert\NotBlank(message="La présentation de l'établissement est obligatoire")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Hours::class, mappedBy="establishment")
     * @Groups({"establishment:default:read"})
     */
    private $openHours;

    /**
     * @ORM\ManyToOne(targetEntity=Department::class, inversedBy="establishments")
     * @Groups({
     *     "establishment:default:list",
     *     "establishment:default:read",
     *     "establishment:manager:write",
     *     "userEstablishment:owner:read"
     * })
     * @Assert\NotBlank(message="Le département de l'établissement est obligatoire")
     */
    private $department;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="ownedEstablishments")
     * @Groups({"establishment:manager:read", "establishment:admin:write"})
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity=Report::class, mappedBy="establishment")
     */
    private $reports;

    /**
     * @ORM\OneToMany(targetEntity=Service::class, mappedBy="establishment")
     * @Groups({"establishment:default:read"})
     */
    private $services;

    /**
     * @ORM\OneToMany(targetEntity=Appointment::class, mappedBy="establishment")
     */
    private $appointments;

    /**
     * @ORM\OneToMany(targetEntity=Review::class, mappedBy="establishment")
     */
    private $reviews;

    /**
     * @ORM\OneToMany(targetEntity=UserEstablishment::class, mappedBy="establishment")
     */
    private $userEstablishments;

    public function __construct()
    {
        $this->openHours = new ArrayCollection();
        $this->reports = new ArrayCollection();
        $this->services = new ArrayCollection();
        $this->appointments = new ArrayCollection();
        $this->reviews = new ArrayCollection();
        $this->userEstablishments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCommercialName(): ?string
    {
        return $this->commercialName;
    }

    public function setCommercialName(?string $commercialName): self
    {
        $this->commercialName = $commercialName;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAddressComplement(): ?string
    {
        return $this->addressComplement;
    }

    public function setAddressComplement(?string $addressComplement): self
    {
        $this->addressComplement = $addressComplement;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(?string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getContactEmail(): ?string
    {
        return $this->contactEmail;
    }

    public function setContactEmail(?string $contactEmail): self
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    public function getIllustration(): ?string
    {
        return $this->illustration;
    }

    public function setIllustration(?string $illustration): self
    {
        $this->illustration = $illustration;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Hours[]
     */
    public function getOpenHours(): Collection
    {
        return $this->openHours;
    }

    public function addOpenHour(Hours $openHour): self
    {
        if (!$this->openHours->contains($openHour)) {
            $this->openHours[] = $openHour;
            $openHour->setEstablishment($this);
        }

        return $this;
    }

    public function removeOpenHour(Hours $openHour): self
    {
        if ($this->openHours->removeElement($openHour)) {
            // set the owning side to null (unless already changed)
            if ($openHour->getEstablishment() === $this) {
                $openHour->setEstablishment(null);
            }
        }

        return $this;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(?Department $department): self
    {
        $this->department = $department;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection|Report[]
     */
    public function getReports(): Collection
    {
        return $this->reports;
    }

    public function addReport(Report $report): self
    {
        if (!$this->reports->contains($report)) {
            $this->reports[] = $report;
            $report->setEstablishment($this);
        }

        return $this;
    }

    public function removeReport(Report $report): self
    {
        if ($this->reports->removeElement($report)) {
            // set the owning side to null (unless already changed)
            if ($report->getEstablishment() === $this) {
                $report->setEstablishment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Service[]
     */
    public function getServices(): Collection
    {
        return $this->services;
    }

    public function addService(Service $service): self
    {
        if (!$this->services->contains($service)) {
            $this->services[] = $service;
            $service->setEstablishment($this);
        }

        return $this;
    }

    public function removeService(Service $service): self
    {
        if ($this->services->removeElement($service)) {
            // set the owning side to null (unless already changed)
            if ($service->getEstablishment() === $this) {
                $service->setEstablishment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Appointment[]
     */
    public function getAppointments(): Collection
    {
        return $this->appointments;
    }

    public function addAppointment(Appointment $appointment): self
    {
        if (!$this->appointments->contains($appointment)) {
            $this->appointments[] = $appointment;
            $appointment->setEstablishment($this);
        }

        return $this;
    }

    public function removeAppointment(Appointment $appointment): self
    {
        if ($this->appointments->removeElement($appointment)) {
            // set the owning side to null (unless already changed)
            if ($appointment->getEstablishment() === $this) {
                $appointment->setEstablishment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Review[]
     */
    public function getReviews(): Collection
    {
        return $this->reviews;
    }

    public function addReview(Review $review): self
    {
        if (!$this->reviews->contains($review)) {
            $this->reviews[] = $review;
            $review->setEstablishment($this);
        }

        return $this;
    }

    public function removeReview(Review $review): self
    {
        if ($this->reviews->removeElement($review)) {
            // set the owning side to null (unless already changed)
            if ($review->getEstablishment() === $this) {
                $review->setEstablishment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserEstablishment[]
     */
    public function getUserEstablishments(): Collection
    {
        return $this->userEstablishments;
    }

    public function addUserEstablishment(UserEstablishment $userEstablishment): self
    {
        if (!$this->userEstablishments->contains($userEstablishment)) {
            $this->userEstablishments[] = $userEstablishment;
            $userEstablishment->setEstablishment($this);
        }

        return $this;
    }

    public function removeUserEstablishment(UserEstablishment $userEstablishment): self
    {
        if ($this->userEstablishments->removeElement($userEstablishment)) {
            // set the owning side to null (unless already changed)
            if ($userEstablishment->getEstablishment() === $this) {
                $userEstablishment->setEstablishment(null);
            }
        }

        return $this;
    }

    /**
     * @return User[]
     * @Groups({"establishment:manager:read"})
     */
    public function getManagers(): array
    {
        $managers = [];
        foreach ($this->userEstablishments as $userEstablishment) {
            if ($userEstablishment->getValidated()) {
                $managers[] = $userEstablishment->getManager();
            }
        }

        return $managers;
    }

    /**
     * @return bool
     * @Groups({"establishment:admin:write", "establishment:admin:read", "establishment:admin:list"})
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param DateTime $date
     * @return mixed|null
     */
    public function getRelatedHour (DateTime $date)
    {
        $dayName = Helper::getFrenchDayName($date->format('l'));
        $matchingHour = null;

        foreach ($this->openHours as $openHour) {
            if ($openHour->getDay()->getName() === $dayName) {
                $isIncludedInShift = $date->format('H:i') >= $openHour->getStartHour() && $date->format('H:i') <= $openHour->getEndHour();
                if ($isIncludedInShift) {
                    $matchingHour = $openHour;
                }
            }
        }

        return $matchingHour;
    }

    /**
     * @param Service $service
     * @return bool|null
     */
    public function doesServiceBelongTo (Service $service): ?bool
    {
        $serviceBelongsToEstablishment = false;

        foreach ($this->services as $currentService) {
            if ($currentService === $service) {
                $serviceBelongsToEstablishment = true;
                break;
            }
        }

        return  $serviceBelongsToEstablishment;
    }

    /**
     * @return float
     * @Groups({"establishment:default:read", "establishment:default:list"})
     */
    public function getMarkAverage(): ?float
    {
        $average = null;
        $reviewNumber = count($this->reviews);

        if ($reviewNumber > 0) {
            $sum = 0;

            foreach ($this->reviews as $review) {
                $sum += $review->getMark();
            }

            $average = $sum / $reviewNumber;
        }


        return $average;
    }
}