<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\DeletedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Repository\AppointmentRepository;
use App\Validator\IsValidAppointment;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\Appointment\GetAllMyAppointmentsAsClientController;
use App\Controller\Appointment\GetMyIncomingAppointmentsAsClientController;
use App\Controller\Appointment\GetTodayAppointmentsAsClientController;
use App\Controller\Appointment\GetAllEstablishmentAppointmentsAsManagerController;
use App\Controller\Appointment\GetIncomingEstablishmentAppointmentsAsManagerController;
use App\Controller\Appointment\GetTodayEstablishmentAppointmentsAsManagerController;
use App\Controller\Appointment\GetEstablishmentAppointmentsAtDateController;
use App\Controller\Appointment\UpdateAppointmentAsClientController;
use App\Controller\Appointment\UpdateAppointmentAsManagerController;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "post"={
 *             "denormalization_context"={"groups"={"appointement:create"}},
 *             "normalization_context"={"groups"={"appointement:read"}},
 *             "security"="is_granted('ROLE_CLIENT')"
 *         },
 *         "get_all_my_appointments_as_client"={
 *              "method"="GET",
 *              "path"="/appointments/client/get-all-my-appointments",
 *              "controller"=GetAllMyAppointmentsAsClientController::class,
 *              "normalization_context"={"groups"={"appointement:read"}},
 *              "security"="is_granted('ROLE_CLIENT')"
 *          },
 *         "get_my_incoming_appointments_as_client"={
 *              "method"="GET",
 *              "path"="/appointments/client/get-my-incoming-appointments",
 *              "controller"=GetMyIncomingAppointmentsAsClientController::class,
 *              "normalization_context"={"groups"={"appointement:read"}},
 *              "security"="is_granted('ROLE_CLIENT')"
 *          },
 *         "get_today_appointments_as_client"={
 *              "method"="GET",
 *              "path"="/appointments/client/get-today-appointments",
 *              "controller"=GetTodayAppointmentsAsClientController::class,
 *              "normalization_context"={"groups"={"appointement:read"}},
 *              "security"="is_granted('ROLE_CLIENT')"
 *          },
 *         "get_all_establishment_appointments_as_manager"={
 *              "method"="GET",
 *              "path"="/appointments/manager/get-all-establishment-appointments",
 *              "controller"=GetAllEstablishmentAppointmentsAsManagerController::class,
 *              "normalization_context"={"groups"={"appointement:read"}},
 *              "security"="is_granted('ROLE_MANAGER')"
 *          },
 *         "get_incoming_establishment_appointments_as_manager"={
 *              "method"="GET",
 *              "path"="/appointments/manager/get-incoming-establishment-appointments",
 *              "controller"=GetIncomingEstablishmentAppointmentsAsManagerController::class,
 *              "normalization_context"={"groups"={"appointement:read"}},
 *              "security"="is_granted('ROLE_MANAGER')"
 *          },
 *         "get_today_establishment_appointments_as_manager"={
 *              "method"="GET",
 *              "path"="/appointments/manager/get-today-establishment-appointments",
 *              "controller"=GetTodayEstablishmentAppointmentsAsManagerController::class,
 *              "normalization_context"={"groups"={"appointement:read"}},
 *              "security"="is_granted('ROLE_MANAGER')"
 *          },
 *         "get_establishment_appointments_at_date"={
 *              "method"="GET",
 *              "path"="/appointments/get-establishment-appointments-at-date",
 *              "controller"=GetEstablishmentAppointmentsAtDateController::class,
 *              "normalization_context"={"groups"={"appointement:read"}},
 *              "security"="is_granted('ROLE_USER')"
 *          }
 *     },
 *     itemOperations={
 *         "get"={
 *             "normalization_context"={"groups"={"appointement:read"}},
 *             "security"="is_granted('READ_USER', object.getClient())"
 *         },
 *         "update_apppointment_as_client"={
 *             "method"="PATCH",
 *             "path"="/appointments/{id}/client/update-appointment",
 *             "controller"=UpdateAppointmentAsClientController::class,
 *             "denormalization_context"={"groups"={"appointement:edit"}},
 *             "normalization_context"={"groups"={"appointement:read"}},
 *             "security"="is_granted('READ_USER', object.getClient())"
 *         },
 *         "update_apppointment_as_manager"={
 *             "method"="PATCH",
 *             "path"="/appointments/{id}/manager/update-appointment",
 *             "controller"=UpdateAppointmentAsManagerController::class,
 *             "denormalization_context"={"groups"={"appointement:edit"}},
 *             "normalization_context"={"groups"={"appointement:read"}},
 *             "security"="is_granted('READ_USER', object.getClient())"
 *         }
 *     }
 * )
 * @ORM\Entity(repositoryClass=AppointmentRepository::class)
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @IsValidAppointment()
 * @ApiFilter(SearchFilter::class, properties={"establishment": "exact"})
 */
class Appointment
{
    use DeletedTrait;
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"appointement:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"appointement:read", "appointement:create"})
     * @Assert\Type(type="datetime", message="La date et l'heure sont invalides")
     */
    private $startHour;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="appointments")
     * @Groups({"appointement:read"})
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity=Establishment::class, inversedBy="appointments")
     * @Groups({"appointement:read", "appointement:create"})
     */
    private $establishment;

    /**
     * @ORM\ManyToOne(targetEntity=AppointmentStatus::class, inversedBy="appointments")
     * @Groups({"appointement:read", "appointement:edit"})
     */
    private $appointmentStatus;

    /**
     * @ORM\ManyToMany(targetEntity=Service::class, inversedBy="appointments")
     * @Groups({"appointement:read", "appointement:create"})
     * @Assert\Count(min="1", minMessage="Veuillez sélectionner au moins une prestation pour ce rendez-vous")
     */
    private $services;

    public function __construct()
    {
        $this->services = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartHour(): ?DateTimeInterface
    {
        return $this->startHour;
    }

    public function setStartHour(?DateTimeInterface $startHour): self
    {
        $this->startHour = $startHour;

        return $this;
    }

    public function getClient(): ?User
    {
        return $this->client;
    }

    public function setClient(?User $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getEstablishment(): ?Establishment
    {
        return $this->establishment;
    }

    public function setEstablishment(?Establishment $establishment): self
    {
        $this->establishment = $establishment;

        return $this;
    }

    public function getAppointmentStatus(): ?AppointmentStatus
    {
        return $this->appointmentStatus;
    }

    public function setAppointmentStatus(?AppointmentStatus $appointmentStatus): self
    {
        $this->appointmentStatus = $appointmentStatus;

        return $this;
    }

    /**
     * @return Collection|Service[]
     */
    public function getServices(): Collection
    {
        return $this->services;
    }

    public function addService(Service $service): self
    {
        if (!$this->services->contains($service)) {
            $this->services[] = $service;
        }

        return $this;
    }

    public function removeService(Service $service): self
    {
        $this->services->removeElement($service);

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     * @throws Exception
     * @Groups({"appointement:read"})
     */
    public function getEndHour (): ?DateTimeInterface
    {
        $minutesToAdd = 0;

        foreach ($this->services as $service) {
            $minutesToAdd += $service->getDuration();
        }

        $endHour = clone $this->getStartHour();
        $endHour->modify('+' . $minutesToAdd . ' minutes');

        return $endHour;
    }
}
