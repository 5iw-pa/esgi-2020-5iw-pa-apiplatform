<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\DeletedTrait;
use App\Repository\UserEstablishmentRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Controller\UserEstablishment\AskJoinEstablishmentAsManagerController;
use App\Controller\UserEstablishment\GetPendingRequestsAsOwnerController;
use App\Controller\UserEstablishment\GetEstablishmentManagerListController;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;


/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={
 *             "normalization_context"={"groups"={"userEstablishment:owner:read"}},
 *             "security"="is_granted('ROLE_ADMIN')"
 *         },
 *         "ask_join_establishment_as_manager"={
 *              "method"="POST",
 *              "path"="/user_establishments/manager/ask-for-join-establishment",
 *              "controller"=AskJoinEstablishmentAsManagerController::class,
 *              "denormalization_context"={"groups"={"userEstablishment:manager:write"}},
 *              "normalization_context"={"groups"={"userEstablishment:manager:read"}},
 *              "security"="is_granted('ROLE_MANAGER')"
 *          },
 *          "get_pending_request_as_owner"={
 *              "method"="GET",
 *              "path"="/user_establishments/owner/get-pending-requests",
 *              "controller"=GetPendingRequestsAsOwnerController::class,
 *              "normalization_context"={"groups"={"userEstablishment:owner:read"}},
 *              "security"="is_granted('ROLE_MANAGER')"
 *          },
 *         "get_establishment_manager_list"={
 *              "method"="GET",
 *              "path"="/user_establishments/owner/get-establishment-manager-list",
 *              "controller"=GetEstablishmentManagerListController::class,
 *              "normalization_context"={"groups"={"userEstablishment:owner:read"}},
 *              "security"="is_granted('ROLE_MANAGER')"
 *          },
 *     },
 *     itemOperations={
 *         "get"={
 *             "normalization_context"={"groups"={"userEstablishment:owner:read"}},
 *             "security"="is_granted('EDIT_ESTABLISHMENT_AS_OWNER', object.getEstablishment())"
 *         },
 *         "patch"={
 *             "denormalization_context"={"groups"={"userEstablishment:owner:write"}},
 *             "normalization_context"={"groups"={"userEstablishment:owner:read"}},
 *             "security"="is_granted('EDIT_ESTABLISHMENT_AS_OWNER', object.getEstablishment())"
 *         },
 *         "delete"={
 *             "security"="is_granted('EDIT_ESTABLISHMENT_AS_OWNER', object.getEstablishment())"
 *         }
 *     }
 * )
 * @ORM\Entity(repositoryClass=UserEstablishmentRepository::class)
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @ApiFilter(SearchFilter::class, properties={"establishment": "exact"})
 */
class UserEstablishment
{
    use DeletedTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"userEstablishment:manager:read", "userEstablishment:owner:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"userEstablishment:owner:read", "userEstablishment:owner:write"})
     *
     */
    private $validated = false;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userEstablishments")
     * @Groups({"userEstablishment:owner:read"})
     */
    private $manager;

    /**
     * @ORM\ManyToOne(targetEntity=Establishment::class, inversedBy="userEstablishments")
     * @Groups({"userEstablishment:owner:read"})
     */
    private $establishment;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValidated(): ?bool
    {
        return $this->validated;
    }

    public function setValidated(?bool $validated): self
    {
        $this->validated = $validated;

        return $this;
    }

    public function getManager(): ?User
    {
        return $this->manager;
    }

    public function setManager(?User $manager): self
    {
        $this->manager = $manager;

        return $this;
    }

    public function getEstablishment(): ?Establishment
    {
        return $this->establishment;
    }

    public function setEstablishment(?Establishment $establishment): self
    {
        $this->establishment = $establishment;

        return $this;
    }
}
