<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\DeletedTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Repository\ReportRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={
 *             "normalization_context"={"groups"={"report:read"}},
 *             "security"="is_granted('ROLE_ADMIN')"
 *         },
 *         "post"={
 *             "denormalization_context"={"groups"={"report:write"}},
 *             "normalization_context"={"groups"={"report:read"}},
 *             "security"="is_granted('ROLE_CLIENT')"
 *         }
 *     },
 *     itemOperations={
 *         "get"={
 *             "normalization_context"={"groups"={"report:read"}},
 *             "security"="is_granted('ROLE_ADMIN')"
 *         },
 *         "delete"={
 *             "security"="is_granted('ROLE_ADMIN')"
 *         }
 *     }
 * )
 * @ORM\Entity(repositoryClass=ReportRepository::class)
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 */
class Report
{
    use DeletedTrait;
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"report:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"report:read", "report:write"})
     * @Assert\NotBlank(message="Le titre du signalement est obligatoire")
     * @Assert\Type(type="string", message="Veuillez saisir un titre correct")
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"report:read", "report:write"})
     * @Assert\NotBlank(message="Le contenu du signalement est obligatoire")
     * @Assert\Type(type="string", message="Veuillez saisir un texte correct")
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="reports")
     * @Groups({"report:read"})
     */
    private $author;

    /**
     * @ORM\ManyToOne(targetEntity=Establishment::class, inversedBy="reports")
     * @Groups({"report:read", "report:write"})
     * @Assert\NotBlank(message="L'établissement est obligatoire")
     */
    private $establishment;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getEstablishment(): ?Establishment
    {
        return $this->establishment;
    }

    public function setEstablishment(?Establishment $establishment): self
    {
        $this->establishment = $establishment;

        return $this;
    }
}
