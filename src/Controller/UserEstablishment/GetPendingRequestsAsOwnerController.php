<?php

namespace App\Controller\UserEstablishment;

use App\Entity\Establishment;
use App\Entity\UserEstablishment;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;

class GetPendingRequestsAsOwnerController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Security
     */
    private $security;

    public function __construct(EntityManagerInterface $entityManager, Security $security)
    {
        $this->em = $entityManager;
        $this->security = $security;
    }

    /**
     * @param Request $request
     * @return UserEstablishment[]
     */
    public function __invoke(Request $request): array
    {
        $parameters = $request->query->all();

        if (!isset($parameters['establishment'])) {
            throw new NotFoundHttpException("Établissement non reconnu");
        }

        $establishmentId = $parameters['establishment'];

        $establishmentRepository = $this->em->getRepository(Establishment::class);

        $establishment = $establishmentRepository->findOneBy([
            'id' => $establishmentId
        ]);

        if (!$establishment) {
            throw new NotFoundHttpException("Établissement non reconnu");
        }

        if (!$this->security->isGranted('EDIT_ESTABLISHMENT_AS_OWNER', $establishment)) {
            throw new AccessDeniedException();
        }

        $userEstablishmentRepository = $this->em->getRepository(UserEstablishment::class);

        $pendingRequests = $userEstablishmentRepository->findBy([
            'establishment' => $establishment,
            'validated' => false
        ]);


        return  $pendingRequests;
    }
}
