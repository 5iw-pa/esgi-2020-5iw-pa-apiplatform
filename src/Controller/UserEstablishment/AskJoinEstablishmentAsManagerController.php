<?php

namespace App\Controller\UserEstablishment;

use App\Entity\Establishment;
use App\Entity\UserEstablishment;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AskJoinEstablishmentAsManagerController
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(TokenStorageInterface $tokenStorage, EntityManagerInterface $entityManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->em = $entityManager;
    }

    /**
     * @param Request $request
     * @param UserEstablishment $data
     * @return UserEstablishment
     */
    public function __invoke(Request $request, UserEstablishment $data): UserEstablishment
    {
        $manager = $this->tokenStorage->getToken()->getUser();

        $bodyData = json_decode($request->getContent());
        $siret = $bodyData->siret;

        $establishmentRepository = $this->em->getRepository(Establishment::class);

        $establishment = $establishmentRepository->findOneBy([
            'siret' => $siret
        ]);

        if (!$establishment) {
            throw new NotFoundHttpException("Établissement non reconnu");
        }

        $data->setEstablishment($establishment)->setManager($manager);

        return $data;
    }
}
