<?php

namespace App\Controller\Establishment;

use App\Entity\Establishment;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

class GetEstablishmentsInactiveController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request $request
     * @return Establishment[]|object[]
     */
    public function __invoke(Request $request): array
    {
        $establishments = $this->entityManager->getRepository(Establishment::class)->findBy(['active' => false]);

        return $establishments;
    }
}
