<?php

namespace App\Controller\Establishment;

use App\Entity\Establishment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class GetMyEstablishmentsControllerAsOwner
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Request $request
     * @return Establishment[]|object[]
     */
    public function __invoke(Request $request): array
    {
        $manager = $this->tokenStorage->getToken()->getUser();

        $establishments = $manager->getOwnedEstablishments()->getValues();


        return $establishments;
    }
}
