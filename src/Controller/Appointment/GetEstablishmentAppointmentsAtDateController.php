<?php

namespace App\Controller\Appointment;

use App\Entity\Appointment;
use App\Entity\Establishment;
use App\Service\Helper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;

class GetEstablishmentAppointmentsAtDateController
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Security
     */
    private $security;

    public function __construct(TokenStorageInterface $tokenStorage, EntityManagerInterface $entityManager, Security $security)
    {
        $this->tokenStorage = $tokenStorage;
        $this->em = $entityManager;
        $this->security = $security;
    }

    /**
     * @param Request $request
     * @return Establishment[]|object[]
     * @throws \Exception
     */
    public function __invoke(Request $request): array
    {
        $parameters = $request->query->all();

        if (!isset($parameters['establishment'])) {
            throw new NotFoundHttpException("Établissement non reconnu");
        }

        if (!isset($parameters['date'])) {
            throw new NotFoundHttpException("Date non renseignée");
        }

        $dateString = $parameters['date'];
        $isDateValid = Helper::validateDate($dateString);

        if (!$isDateValid) {
            throw new HttpException(400, "Cette date n'est pas valide");
        }

        if ($dateString < (new \DateTime('now'))->format('d-m-Y')) {
            throw new HttpException(400, "La date ne pas être antérieur à celle d'aujourd'hui");
        }

        $establishmentId = $parameters['establishment'];

        $establishmentRepository = $this->em->getRepository(Establishment::class);

        $establishment = $establishmentRepository->findOneBy([
            'id' => $establishmentId
        ]);

        if (!$establishment) {
            throw new NotFoundHttpException("Établissement non reconnu");
        }

        $appointmentRepository = $this->em->getRepository(Appointment::class);

        $appointments = $appointmentRepository->getEstablishmentAppointmentAtDate($establishment, $dateString);


        return $appointments;
    }
}
