<?php

namespace App\Controller\Appointment;

use App\Entity\Appointment;
use App\Entity\Establishment;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;

class GetIncomingEstablishmentAppointmentsAsManagerController
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Security
     */
    private $security;

    public function __construct(TokenStorageInterface $tokenStorage, EntityManagerInterface $entityManager, Security $security)
    {
        $this->tokenStorage = $tokenStorage;
        $this->em = $entityManager;
        $this->security = $security;
    }

    /**
     * @param Request $request
     * @return Establishment[]|object[]
     */
    public function __invoke(Request $request): array
    {
        $parameters = $request->query->all();

        if (!isset($parameters['establishment'])) {
            throw new NotFoundHttpException("Établissement non reconnu");
        }

        $establishmentId = $parameters['establishment'];

        $establishmentRepository = $this->em->getRepository(Establishment::class);

        $establishment = $establishmentRepository->findOneBy([
            'id' => $establishmentId
        ]);

        if (!$establishment) {
            throw new NotFoundHttpException("Établissement non reconnu");
        }

        if (!$this->security->isGranted('READ_ESTABLISHMENT_AS_MANAGER', $establishment)) {
            throw new AccessDeniedException();
        }

        $appointmentRepository = $this->em->getRepository(Appointment::class);

        $appointments = $appointmentRepository->getIncomingEstablishmentAppointmentsAsManager($establishment);


        return $appointments;
    }
}
