<?php

namespace App\Controller\Appointment;

use App\Entity\Appointment;
use App\Entity\AppointmentStatus;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UpdateAppointmentAsClientController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param Request $request
     * @param Appointment $data
     * @return Appointment
     */
    public function __invoke(Request $request, Appointment $data): Appointment
    {

        $statusRequired = $data->getAppointmentStatus();

        $cancelStatus = $this->em->getRepository(AppointmentStatus::class)->findOneBy([
            'name' => 'Annulé'
        ]);

        if ($statusRequired !== $cancelStatus) {
            throw new AccessDeniedException("Vous n'êtes pas autorisé à passer le statut du rendez-vous à '" . $statusRequired->getName() . "'");
        }

        return $data;
    }
}
