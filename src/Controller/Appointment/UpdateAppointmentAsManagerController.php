<?php

namespace App\Controller\Appointment;

use App\Entity\Appointment;
use App\Entity\AppointmentStatus;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;

class UpdateAppointmentAsManagerController
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Security
     */
    private $security;

    public function __construct(TokenStorageInterface $tokenStorage, EntityManagerInterface $entityManager, Security $security)
    {
        $this->tokenStorage = $tokenStorage;
        $this->em = $entityManager;
        $this->security = $security;
    }

    /**
     * @param Request $request
     * @param Appointment $data
     * @return Appointment
     */
    public function __invoke(Request $request, Appointment $data): Appointment
    {

        if (!$this->security->isGranted('ROLE_MANAGER')) {
            throw new AccessDeniedException();
        }

        $statusRequired = $data->getAppointmentStatus();

        // TODO get appointment data before persist, to be able to add rules for preventing status to become inconsistent
        // example: go to an in progress status, from a finished status
        // TODO this $unModifiedData is already hydrated by data from front, find a way to get original data
//        $unModifiedData = $this->em->getRepository(Appointment::class)->findOneBy([
//            'id' => $data->getId()
//        ]);
//
//        $cancelStatus = $this->em->getRepository(AppointmentStatus::class)->findOneBy([
//            'name' => 'Annulé'
//        ]);
//
//        $finishedStatus = $this->em->getRepository(AppointmentStatus::class)->findOneBy([
//            'name' => 'Terminé'
//        ]);
//
//        $inProgressStatus = $this->em->getRepository(AppointmentStatus::class)->findOneBy([
//            'name' => 'En cours'
//        ]);
//
//        $incomingStatus = $this->em->getRepository(AppointmentStatus::class)->findOneBy([
//            'name' => 'Prévu'
//        ]);
//
//        dump($unModifiedData);
//
//        $cantBeUpdated = ($unModifiedData->getAppointmentStatus() === $cancelStatus) || ($unModifiedData->getAppointmentStatus() === $finishedStatus);
//        if ($cantBeUpdated) {
//            throw new AccessDeniedException("Vous n'êtes pas autorisé à passer le statut du rendez-vous à '" . $statusRequired->getName() . "'");
//        }
//
//
//        if ($unModifiedData->getAppointmentStatus() === $incomingStatus) {
//
//            if ($statusRequired !== $inProgressStatus) {
//                throw new AccessDeniedException("Vous n'êtes pas autorisé à passer le statut du rendez-vous à '" . $statusRequired->getName() . "'");
//            }
//        }
//
//        if ($unModifiedData->getAppointmentStatus() === $inProgressStatus) {
//            dump('ok');
//
//            if ($statusRequired !== $finishedStatus) {
//                throw new AccessDeniedException("Vous n'êtes pas autorisé à passer le statut du rendez-vous à '" . $statusRequired->getName() . "'");
//            }
//        }

        return $data;
    }
}
