<?php

namespace App\Controller\Appointment;

use App\Entity\Appointment;
use App\Entity\Establishment;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class GetTodayAppointmentsAsClientController
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(TokenStorageInterface $tokenStorage, EntityManagerInterface $entityManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->em = $entityManager;
    }

    /**
     * @param Request $request
     * @return Establishment[]|object[]
     */
    public function __invoke(Request $request): array
    {
        $client = $this->tokenStorage->getToken()->getUser();

        $appointmentRepository = $this->em->getRepository(Appointment::class);

        $appointments = $appointmentRepository->getTodayAppointmentsAsClient($client);


        return $appointments;
    }
}
