<?php

namespace App\Controller\User;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterManagerController
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;

    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    /**
     * @param Request $request
     * @param User $data
     * @return User
     */
    public function __invoke(Request $request, User $data): User
    {
        $userRoles = ["ROLE_MANAGER"];

        $data->setRoles($userRoles);
        $data->setActive(true);

        return $data;
    }
}
