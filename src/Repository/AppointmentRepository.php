<?php

namespace App\Repository;

use App\Entity\Appointment;
use App\Entity\Establishment;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Appointment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Appointment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Appointment[]    findAll()
 * @method Appointment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AppointmentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Appointment::class);
    }

    // /**
    //  * @return Appointment[] Returns an array of Appointment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Appointment
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @param Establishment $establishment
     * @param DateTime $date
     * @return int|mixed|string
     */
    public function getAppointmentsAtDate(Establishment $establishment, DateTime $date)
    {
        return $this->createQueryBuilder('a')
            ->where('DATE_FORMAT(a.startHour, \'DD-MM-YYYY\') = :date')
            ->andWhere('a.establishment = :establishment')
            ->setParameter('date', $date->format('d-m-Y'))
            ->setParameter('establishment', $establishment)
            ->orderBy('a.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param User $client
     * @return int|mixed|string
     */
    public function getAllMyAppointmentAsClient(User $client)
    {
        return $this->createQueryBuilder('a')
            ->where('a.client = :client')
            ->setParameter('client', $client)
            ->orderBy('a.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param User $client
     * @return int|mixed|string
     */
    public function getMyIncomingAppointmentsAsClient(User $client)
    {
        return $this->createQueryBuilder('a')
            ->join('a.appointmentStatus','appointmentStatus')
            ->where('a.client = :client')
            ->andWhere('appointmentStatus.name = :status')
            ->andWhere('a.startHour >= :today')
            ->setParameter('client', $client)
            ->setParameter('status', 'Prévu')
            ->setParameter('today', new DateTime('now'))
            ->orderBy('a.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param User $client
     * @return int|mixed|string
     */
    public function getTodayAppointmentsAsClient(User $client)
    {
        $today = new DateTime('now');

        return $this->createQueryBuilder('a')
            ->where('a.client = :client')
            ->andWhere('DATE_FORMAT(a.startHour, \'DD-MM-YYYY\') = :today')
            ->setParameter('client', $client)
            ->setParameter('today', $today->format('d-m-Y'))
            ->orderBy('a.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param Establishment $establishment
     * @return int|mixed|string
     */
    public function getAllEstablishmentAppointmentsAsManager(Establishment $establishment)
    {
        return $this->createQueryBuilder('a')
            ->where('a.establishment = :establishment')
            ->setParameter('establishment', $establishment)
            ->orderBy('a.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param Establishment $establishment
     * @return int|mixed|string
     */
    public function getIncomingEstablishmentAppointmentsAsManager(Establishment $establishment)
    {
        return $this->createQueryBuilder('a')
            ->join('a.appointmentStatus','appointmentStatus')
            ->where('a.establishment = :establishment')
            ->andWhere('appointmentStatus.name = :status')
            ->andWhere('a.startHour >= :today')
            ->setParameter('establishment', $establishment)
            ->setParameter('status', 'Prévu')
            ->setParameter('today', new DateTime('now'))
            ->orderBy('a.id', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param Establishment $establishment
     * @return int|mixed|string
     */
    public function getTodayEstablishmentAppointmentsAsManager(Establishment $establishment)
    {
        $today = new DateTime('now');

        return $this->createQueryBuilder('a')
            ->where('a.establishment = :establishment')
            ->andWhere('DATE_FORMAT(a.startHour, \'DD-MM-YYYY\') = :today')
            ->setParameter('establishment', $establishment)
            ->setParameter('today', $today->format('d-m-Y'))
            ->orderBy('a.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param Establishment $establishment
     * @param String $dateString
     * @return int|mixed|string
     */
    public function getEstablishmentAppointmentAtDate(Establishment $establishment, String $dateString)
    {
        return $this->createQueryBuilder('a')
            ->join('a.appointmentStatus','appointmentStatus')
            ->where('a.establishment = :establishment')
            ->andWhere('DATE_FORMAT(a.startHour, \'DD-MM-YYYY\') = :date')
            ->andWhere('appointmentStatus.name = :status')
            ->setParameter('establishment', $establishment)
            ->setParameter('date', $dateString)
            ->setParameter('status', 'Prévu')
            ->orderBy('a.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}
