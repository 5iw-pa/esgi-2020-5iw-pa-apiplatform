<?php

namespace App\Repository;

use App\Entity\Day;
use App\Entity\Establishment;
use App\Entity\Hours;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Hours|null find($id, $lockMode = null, $lockVersion = null)
 * @method Hours|null findOneBy(array $criteria, array $orderBy = null)
 * @method Hours[]    findAll()
 * @method Hours[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HoursRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Hours::class);
    }

    // /**
    //  * @return Hours[] Returns an array of Hours objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Hours
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @param Establishment $establishment
     * @param Day $day
     * @return Hours|null Returns an array of Hours objects
     * @throws NonUniqueResultException
     */
    public function getMorningHoursFromEstablishment(Establishment $establishment, Day $day): ?Hours
    {
        return $this->createQueryBuilder('h')
            ->leftJoin('h.typeOfShift', 'typeOfShift')
            ->andWhere('h.establishment = :establishment')
            ->andWhere('h.day = :day')
            ->andWhere('typeOfShift.name = :typeOfShiftName')
            ->setParameter('establishment', $establishment)
            ->setParameter('day', $day)
            ->setParameter('typeOfShiftName', 'Matin')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    /**
     * @param Establishment $establishment
     * @param Day $day
     * @return Hours|null Returns an array of Hours objects
     * @throws NonUniqueResultException
     */
    public function getAfternoonHoursFromEstablishment(Establishment $establishment, Day $day): ?Hours
    {
        return $this->createQueryBuilder('h')
            ->leftJoin('h.typeOfShift', 'typeOfShift')
            ->andWhere('h.establishment = :establishment')
            ->andWhere('h.day = :day')
            ->andWhere('typeOfShift.name = :typeOfShiftName')
            ->setParameter('establishment', $establishment)
            ->setParameter('day', $day)
            ->setParameter('typeOfShiftName', 'Après-midi')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}
