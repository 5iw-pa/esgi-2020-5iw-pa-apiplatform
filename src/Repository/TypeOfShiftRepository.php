<?php

namespace App\Repository;

use App\Entity\TypeOfShift;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeOfShift|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeOfShift|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeOfShift[]    findAll()
 * @method TypeOfShift[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeOfShiftRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeOfShift::class);
    }

    // /**
    //  * @return TypeOfShift[] Returns an array of TypeOfShift objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeOfShift
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
