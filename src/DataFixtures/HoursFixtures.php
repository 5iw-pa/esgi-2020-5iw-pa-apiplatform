<?php

namespace App\DataFixtures;

use App\Entity\Day;
use App\Entity\Establishment;
use App\Entity\Hours;
use App\Entity\TypeOfShift;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class HoursFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // establishment
        $establishmentParis = $manager->getRepository(Establishment::class)->findOneBy([
            'city' => 'Paris'
        ]);


        //days
        $monday = $manager->getRepository(Day::class)->findOneBy([
            'name' => 'Lundi'
        ]);

        $tuesday = $manager->getRepository(Day::class)->findOneBy([
            'name' => 'Mardi'
        ]);

        $wednesday = $manager->getRepository(Day::class)->findOneBy([
            'name' => 'Mercredi'
        ]);

        $thursday = $manager->getRepository(Day::class)->findOneBy([
            'name' => 'Jeudi'
        ]);

        $friday = $manager->getRepository(Day::class)->findOneBy([
            'name' => 'Vendredi'
        ]);

        $saturday = $manager->getRepository(Day::class)->findOneBy([
            'name' => 'Samedi'
        ]);

        $sunday = $manager->getRepository(Day::class)->findOneBy([
            'name' => 'Dimanche'
        ]);


        // shifts type
        $morningShift = $manager->getRepository(TypeOfShift::class)->findOneBy([
            'name' => 'Matin'
        ]);

        $afternoonShift = $manager->getRepository(TypeOfShift::class)->findOneBy([
            'name' => 'Après-midi'
        ]);

        $fullDayShift = $manager->getRepository(TypeOfShift::class)->findOneBy([
            'name' => 'Journée entière'
        ]);


        $mondayMorning = new Hours();
        $mondayMorning
            ->setEstablishment($establishmentParis)
            ->setDay($monday)
            ->setTypeOfShift($morningShift)
            ->setNbBarber(2)
            ->setStartHour(new \DateTime('2020-01-01 09:00'))
            ->setEndHour(new \DateTime('2020-01-01 13:00'))
        ;
        $manager->persist($mondayMorning);

        $mondayAfternoon = new Hours();
        $mondayAfternoon
            ->setEstablishment($establishmentParis)
            ->setDay($monday)
            ->setTypeOfShift($afternoonShift)
            ->setNbBarber(3)
            ->setStartHour(new \DateTime('2020-01-01 14:00'))
            ->setEndHour(new \DateTime('2020-01-01 19:00'))
        ;
        $manager->persist($mondayAfternoon);


        $tuesdayMorning = new Hours();
        $tuesdayMorning
            ->setEstablishment($establishmentParis)
            ->setDay($tuesday)
            ->setTypeOfShift($morningShift)
            ->setNbBarber(2)
            ->setStartHour(new \DateTime('2020-01-01 09:00'))
            ->setEndHour(new \DateTime('2020-01-01 13:00'))
        ;
        $manager->persist($tuesdayMorning);

        $tuesdayAfternoon = new Hours();
        $tuesdayAfternoon
            ->setEstablishment($establishmentParis)
            ->setDay($tuesday)
            ->setTypeOfShift($afternoonShift)
            ->setNbBarber(3)
            ->setStartHour(new \DateTime('2020-01-01 14:00'))
            ->setEndHour(new \DateTime('2020-01-01 19:00'))
        ;
        $manager->persist($tuesdayAfternoon);


        $wednesdayMorning = new Hours();
        $wednesdayMorning
            ->setEstablishment($establishmentParis)
            ->setDay($wednesday)
            ->setTypeOfShift($morningShift)
            ->setNbBarber(2)
            ->setStartHour(new \DateTime('2020-01-01 09:00'))
            ->setEndHour(new \DateTime('2020-01-01 13:00'))
        ;
        $manager->persist($wednesdayMorning);

        $wednesdayAfternoon = new Hours();
        $wednesdayAfternoon
            ->setEstablishment($establishmentParis)
            ->setDay($wednesday)
            ->setTypeOfShift($afternoonShift)
            ->setNbBarber(3)
            ->setStartHour(new \DateTime('2020-01-01 14:00'))
            ->setEndHour(new \DateTime('2020-01-01 19:00'))
        ;
        $manager->persist($wednesdayAfternoon);


        $thursdayMorning = new Hours();
        $thursdayMorning
            ->setEstablishment($establishmentParis)
            ->setDay($thursday)
            ->setTypeOfShift($morningShift)
            ->setNbBarber(2)
            ->setStartHour(new \DateTime('2020-01-01 09:00'))
            ->setEndHour(new \DateTime('2020-01-01 13:00'))
        ;
        $manager->persist($thursdayMorning);

        $thursdayAfternoon = new Hours();
        $thursdayAfternoon
            ->setEstablishment($establishmentParis)
            ->setDay($thursday)
            ->setTypeOfShift($afternoonShift)
            ->setNbBarber(3)
            ->setStartHour(new \DateTime('2020-01-01 14:00'))
            ->setEndHour(new \DateTime('2020-01-01 19:00'))
        ;
        $manager->persist($thursdayAfternoon);


        $fridayMorning = new Hours();
        $fridayMorning
            ->setEstablishment($establishmentParis)
            ->setDay($friday)
            ->setTypeOfShift($morningShift)
            ->setNbBarber(2)
            ->setStartHour(new \DateTime('2020-01-01 09:00'))
            ->setEndHour(new \DateTime('2020-01-01 13:00'))
        ;
        $manager->persist($fridayMorning);

        $fridayAfternoon = new Hours();
        $fridayAfternoon
            ->setEstablishment($establishmentParis)
            ->setDay($friday)
            ->setTypeOfShift($afternoonShift)
            ->setNbBarber(3)
            ->setStartHour(new \DateTime('2020-01-01 14:00'))
            ->setEndHour(new \DateTime('2020-01-01 19:00'))
        ;
        $manager->persist($fridayAfternoon);


        $saturdayMorning = new Hours();
        $saturdayMorning
            ->setEstablishment($establishmentParis)
            ->setDay($saturday)
            ->setTypeOfShift($morningShift)
            ->setNbBarber(3)
            ->setStartHour(new \DateTime('2020-01-01 09:00'))
            ->setEndHour(new \DateTime('2020-01-01 13:00'))
        ;
        $manager->persist($saturdayMorning);

        $saturdayAfternoon = new Hours();
        $saturdayAfternoon
            ->setEstablishment($establishmentParis)
            ->setDay($saturday)
            ->setTypeOfShift($afternoonShift)
            ->setNbBarber(4)
            ->setStartHour(new \DateTime('2020-01-01 13:00'))
            ->setEndHour(new \DateTime('2020-01-01 19:00'))
        ;
        $manager->persist($saturdayAfternoon);

        $sundayFullDay = new Hours();
        $sundayFullDay
            ->setEstablishment($establishmentParis)
            ->setDay($sunday)
            ->setTypeOfShift($fullDayShift)
            ->setNbBarber(4)
            ->setStartHour(new \DateTime('2020-01-01 11:00'))
            ->setEndHour(new \DateTime('2020-01-01 18:00'))
        ;
        $manager->persist($sundayFullDay);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return array(
            EstablishmentFixtures::class,
            UserFixtures::class
        );
    }
}
