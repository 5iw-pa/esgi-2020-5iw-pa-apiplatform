<?php

namespace App\DataFixtures;

use App\Entity\Establishment;
use App\Entity\Service;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ServiceFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $establishmentParis = $manager->getRepository(Establishment::class)->findOneBy([
            'city' => 'Paris'
        ]);

        $ownerParis = $manager->getRepository(User::class)->findOneBy([
            'email' => 'owner75@gmail.com'
        ]);

        $names = ['Dégradé', 'Shampoing', 'Barbe', 'Teinture', 'Lissage'];
        $prices = [15, 5, 10, 25, 50];
        $durations = [15, 5, 5, 30, 30];

        for ($i = 0; $i < count($names); $i++) {
            $service = new Service();
            $service
                ->setEstablishment($establishmentParis)
                ->setCreatedBy($ownerParis)
                ->setUpdatedBy($ownerParis)
                ->setActive(true)
                ->setName($names[$i])
                ->setPrice($prices[$i])
                ->setDuration($durations[$i])
            ;
            $manager->persist($service);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return array(
            EstablishmentFixtures::class,
            UserFixtures::class
        );
    }
}
