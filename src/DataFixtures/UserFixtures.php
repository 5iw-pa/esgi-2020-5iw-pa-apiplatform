<?php

namespace App\DataFixtures;

use App\Entity\Department;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $department1 = $manager->getRepository(Department::class)->findOneBy([
            'departmentCode' => 75
        ]);

        $department2 = $manager->getRepository(Department::class)->findOneBy([
            'departmentCode' => 13
        ]);

        $admin = new User();
        $admin
            ->setEmail('admin@gmail.com')
            ->setLastname('admin')
            ->setFirstname('admin')
            ->setPassword('azerty')
            ->setGender('Homme')
            ->setDepartment($department1)
            ->setRoles(['ROLE_ADMIN'])
        ;

        $ownerParis = new User();
        $ownerParis
            ->setEmail('owner75@gmail.com')
            ->setLastname('Propriétaire')
            ->setFirstname('établissement Paris')
            ->setPassword('azerty')
            ->setGender('Homme')
            ->setDepartment($department1)
            ->setRoles(['ROLE_MANAGER'])
        ;

        $managerParis = new User();
        $managerParis
            ->setEmail('manager75@gmail.com')
            ->setLastname('Manager')
            ->setFirstname('établissement Paris')
            ->setPassword('azerty')
            ->setGender('Homme')
            ->setDepartment($department1)
            ->setRoles(['ROLE_MANAGER'])
        ;

        $ownerAndManagerMarseille = new User();
        $ownerAndManagerMarseille
            ->setEmail('ownerandmanager13@gmail.com')
            ->setLastname('Propriétaire')
            ->setFirstname('établissement Paris')
            ->setPassword('azerty')
            ->setGender('Homme')
            ->setDepartment($department2)
            ->setRoles(['ROLE_MANAGER'])
        ;

        $manager->persist($admin);
        $manager->persist($ownerParis);
        $manager->persist($managerParis);
        $manager->persist($ownerAndManagerMarseille);

        for ($i = 0; $i < 50; $i++) {
            $client = new User();
            $client
                ->setFirstname($faker->firstName)
                ->setLastname($faker->lastName)
                ->setEmail($faker->email)
                ->setPassword('azerty')
                ->setBirthdate($faker->dateTimeBetween('-50 years', '-16 years'))
                ->setGender($faker->title)
                ->setPhoneNumber($faker->phoneNumber)
                ->setRoles(['ROLE_CLIENT'])
            ;

            if ($i % 2 === 0)
                $client->setDepartment($department1);
            else
                $client->setDepartment($department2);

            $manager->persist($client);
        }


        $manager->flush();
    }
}
