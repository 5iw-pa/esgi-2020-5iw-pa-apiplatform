<?php

namespace App\DataFixtures;

use App\Entity\Department;
use App\Entity\Establishment;
use App\Entity\User;
use App\Entity\UserEstablishment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class EstablishmentFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $department1 = $manager->getRepository(Department::class)->findOneBy([
            'departmentCode' => 75
        ]);

        $department2 = $manager->getRepository(Department::class)->findOneBy([
            'departmentCode' => 13
        ]);


        $ownerParis = $manager->getRepository(User::class)->findOneBy([
            'email' => 'owner75@gmail.com'
        ]);

        $managerParis = $manager->getRepository(User::class)->findOneBy([
            'email' => 'manager75@gmail.com'
        ]);

        $ownerAndManagerMarseille = $manager->getRepository(User::class)->findOneBy([
            'email' => 'ownerandmanager13@gmail.com'
        ]);

        $establishmentParis = new Establishment();
        $establishmentParis
            ->setCommercialName($faker->company)
            ->setAddress($faker->streetAddress)
            ->setAddressComplement($faker->secondaryAddress)
            ->setCity('Paris')
            ->setZipCode('75000')
            ->setSiret($faker->siret)
            ->setPhoneNumber($faker->phoneNumber)
            ->setContactEmail($faker->companyEmail)
            ->setName($faker->catchPhrase)
            ->setDescription($faker->realText())
            ->setDepartment($department1)
            ->setOwner($ownerParis)
            ->setActive(true)
        ;

        $establishmentMarseille = new Establishment();
        $establishmentMarseille
            ->setCommercialName($faker->company)
            ->setAddress($faker->streetAddress)
            ->setAddressComplement($faker->secondaryAddress)
            ->setCity('Marseille')
            ->setZipCode('13000')
            ->setSiret($faker->siret)
            ->setPhoneNumber($faker->phoneNumber)
            ->setContactEmail($faker->companyEmail)
            ->setName($faker->catchPhrase)
            ->setDescription($faker->realText())
            ->setDepartment($department2)
            ->setOwner($ownerAndManagerMarseille)
            ->setActive(true)
        ;

        $parisManagerRight = new UserEstablishment();
        $parisManagerRight
            ->setManager($managerParis)
            ->setEstablishment($establishmentParis)
            ->setValidated(true)
        ;

        $marseilleManagerRight = new UserEstablishment();
        $marseilleManagerRight
            ->setManager($ownerAndManagerMarseille)
            ->setEstablishment($establishmentParis)
            ->setValidated(true)
        ;

        $manager->persist($establishmentParis);
        $manager->persist($establishmentMarseille);
        $manager->persist($parisManagerRight);
        $manager->persist($marseilleManagerRight);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return array(
            UserFixtures::class,
        );
    }
}
