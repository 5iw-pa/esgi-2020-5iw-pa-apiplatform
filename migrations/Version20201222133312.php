<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201222133312 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE appointment_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE appointment_status_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE day_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE department_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE establishment_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE hours_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE report_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE review_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE service_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE type_of_shift_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE user_establishment_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE appointment (id INT NOT NULL, client_id INT DEFAULT NULL, establishment_id INT DEFAULT NULL, appointment_status_id INT DEFAULT NULL, start_hour TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, end_hour TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, deleted BOOLEAN DEFAULT \'false\' NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FE38F84419EB6921 ON appointment (client_id)');
        $this->addSql('CREATE INDEX IDX_FE38F8448565851 ON appointment (establishment_id)');
        $this->addSql('CREATE INDEX IDX_FE38F844FE7D6EB4 ON appointment (appointment_status_id)');
        $this->addSql('CREATE TABLE appointment_service (appointment_id INT NOT NULL, service_id INT NOT NULL, PRIMARY KEY(appointment_id, service_id))');
        $this->addSql('CREATE INDEX IDX_70BEA8FAE5B533F9 ON appointment_service (appointment_id)');
        $this->addSql('CREATE INDEX IDX_70BEA8FAED5CA9E6 ON appointment_service (service_id)');
        $this->addSql('CREATE TABLE appointment_status (id INT NOT NULL, name VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE day (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE department (id INT NOT NULL, name VARCHAR(255) DEFAULT NULL, department_code INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE establishment (id INT NOT NULL, department_id INT DEFAULT NULL, owner_id INT DEFAULT NULL, commercial_name VARCHAR(255) DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, address_complement VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, zip_code VARCHAR(255) DEFAULT NULL, siret VARCHAR(255) DEFAULT NULL, phone_number VARCHAR(255) DEFAULT NULL, contact_email VARCHAR(255) DEFAULT NULL, illustration VARCHAR(255) DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, description TEXT DEFAULT NULL, active BOOLEAN DEFAULT \'false\' NOT NULL, deleted BOOLEAN DEFAULT \'false\' NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DBEFB1EEAE80F5DF ON establishment (department_id)');
        $this->addSql('CREATE INDEX IDX_DBEFB1EE7E3C61F9 ON establishment (owner_id)');
        $this->addSql('CREATE TABLE hours (id INT NOT NULL, establishment_id INT DEFAULT NULL, day_id INT DEFAULT NULL, type_of_shift_id INT DEFAULT NULL, start_hour TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, end_hour TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, nb_barber INT DEFAULT NULL, active BOOLEAN DEFAULT \'false\' NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8A1ABD8D8565851 ON hours (establishment_id)');
        $this->addSql('CREATE INDEX IDX_8A1ABD8D9C24126 ON hours (day_id)');
        $this->addSql('CREATE INDEX IDX_8A1ABD8D4CF29D74 ON hours (type_of_shift_id)');
        $this->addSql('CREATE TABLE report (id INT NOT NULL, author_id INT DEFAULT NULL, establishment_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, content TEXT DEFAULT NULL, deleted BOOLEAN DEFAULT \'false\' NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C42F7784F675F31B ON report (author_id)');
        $this->addSql('CREATE INDEX IDX_C42F77848565851 ON report (establishment_id)');
        $this->addSql('CREATE TABLE review (id INT NOT NULL, author_id INT DEFAULT NULL, establishment_id INT DEFAULT NULL, mark INT DEFAULT NULL, content TEXT DEFAULT NULL, deleted BOOLEAN DEFAULT \'false\' NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_794381C6F675F31B ON review (author_id)');
        $this->addSql('CREATE INDEX IDX_794381C68565851 ON review (establishment_id)');
        $this->addSql('CREATE TABLE service (id INT NOT NULL, establishment_id INT DEFAULT NULL, created_by_id INT DEFAULT NULL, updated_by_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, price DOUBLE PRECISION DEFAULT NULL, duration INT DEFAULT NULL, illustration VARCHAR(255) DEFAULT NULL, active BOOLEAN DEFAULT \'false\' NOT NULL, deleted BOOLEAN DEFAULT \'false\' NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E19D9AD28565851 ON service (establishment_id)');
        $this->addSql('CREATE INDEX IDX_E19D9AD2B03A8386 ON service (created_by_id)');
        $this->addSql('CREATE INDEX IDX_E19D9AD2896DBBDE ON service (updated_by_id)');
        $this->addSql('CREATE TABLE type_of_shift (id INT NOT NULL, name VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, department_id INT DEFAULT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, plain_password VARCHAR(255) DEFAULT NULL, firstname VARCHAR(255) DEFAULT NULL, lastname VARCHAR(255) DEFAULT NULL, birthdate DATE DEFAULT NULL, gender VARCHAR(255) DEFAULT NULL, phone_number VARCHAR(255) DEFAULT NULL, active BOOLEAN DEFAULT \'false\' NOT NULL, deleted BOOLEAN DEFAULT \'false\' NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('CREATE INDEX IDX_8D93D649AE80F5DF ON "user" (department_id)');
        $this->addSql('CREATE TABLE user_establishment (id INT NOT NULL, manager_id INT DEFAULT NULL, establishment_id INT DEFAULT NULL, validated BOOLEAN DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_35611CBE783E3463 ON user_establishment (manager_id)');
        $this->addSql('CREATE INDEX IDX_35611CBE8565851 ON user_establishment (establishment_id)');
        $this->addSql('ALTER TABLE appointment ADD CONSTRAINT FK_FE38F84419EB6921 FOREIGN KEY (client_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE appointment ADD CONSTRAINT FK_FE38F8448565851 FOREIGN KEY (establishment_id) REFERENCES establishment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE appointment ADD CONSTRAINT FK_FE38F844FE7D6EB4 FOREIGN KEY (appointment_status_id) REFERENCES appointment_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE appointment_service ADD CONSTRAINT FK_70BEA8FAE5B533F9 FOREIGN KEY (appointment_id) REFERENCES appointment (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE appointment_service ADD CONSTRAINT FK_70BEA8FAED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE establishment ADD CONSTRAINT FK_DBEFB1EEAE80F5DF FOREIGN KEY (department_id) REFERENCES department (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE establishment ADD CONSTRAINT FK_DBEFB1EE7E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE hours ADD CONSTRAINT FK_8A1ABD8D8565851 FOREIGN KEY (establishment_id) REFERENCES establishment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE hours ADD CONSTRAINT FK_8A1ABD8D9C24126 FOREIGN KEY (day_id) REFERENCES day (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE hours ADD CONSTRAINT FK_8A1ABD8D4CF29D74 FOREIGN KEY (type_of_shift_id) REFERENCES type_of_shift (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F7784F675F31B FOREIGN KEY (author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F77848565851 FOREIGN KEY (establishment_id) REFERENCES establishment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE review ADD CONSTRAINT FK_794381C6F675F31B FOREIGN KEY (author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE review ADD CONSTRAINT FK_794381C68565851 FOREIGN KEY (establishment_id) REFERENCES establishment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD28565851 FOREIGN KEY (establishment_id) REFERENCES establishment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD2B03A8386 FOREIGN KEY (created_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD2896DBBDE FOREIGN KEY (updated_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" ADD CONSTRAINT FK_8D93D649AE80F5DF FOREIGN KEY (department_id) REFERENCES department (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_establishment ADD CONSTRAINT FK_35611CBE783E3463 FOREIGN KEY (manager_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_establishment ADD CONSTRAINT FK_35611CBE8565851 FOREIGN KEY (establishment_id) REFERENCES establishment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE appointment_service DROP CONSTRAINT FK_70BEA8FAE5B533F9');
        $this->addSql('ALTER TABLE appointment DROP CONSTRAINT FK_FE38F844FE7D6EB4');
        $this->addSql('ALTER TABLE hours DROP CONSTRAINT FK_8A1ABD8D9C24126');
        $this->addSql('ALTER TABLE establishment DROP CONSTRAINT FK_DBEFB1EEAE80F5DF');
        $this->addSql('ALTER TABLE "user" DROP CONSTRAINT FK_8D93D649AE80F5DF');
        $this->addSql('ALTER TABLE appointment DROP CONSTRAINT FK_FE38F8448565851');
        $this->addSql('ALTER TABLE hours DROP CONSTRAINT FK_8A1ABD8D8565851');
        $this->addSql('ALTER TABLE report DROP CONSTRAINT FK_C42F77848565851');
        $this->addSql('ALTER TABLE review DROP CONSTRAINT FK_794381C68565851');
        $this->addSql('ALTER TABLE service DROP CONSTRAINT FK_E19D9AD28565851');
        $this->addSql('ALTER TABLE user_establishment DROP CONSTRAINT FK_35611CBE8565851');
        $this->addSql('ALTER TABLE appointment_service DROP CONSTRAINT FK_70BEA8FAED5CA9E6');
        $this->addSql('ALTER TABLE hours DROP CONSTRAINT FK_8A1ABD8D4CF29D74');
        $this->addSql('ALTER TABLE appointment DROP CONSTRAINT FK_FE38F84419EB6921');
        $this->addSql('ALTER TABLE establishment DROP CONSTRAINT FK_DBEFB1EE7E3C61F9');
        $this->addSql('ALTER TABLE report DROP CONSTRAINT FK_C42F7784F675F31B');
        $this->addSql('ALTER TABLE review DROP CONSTRAINT FK_794381C6F675F31B');
        $this->addSql('ALTER TABLE service DROP CONSTRAINT FK_E19D9AD2B03A8386');
        $this->addSql('ALTER TABLE service DROP CONSTRAINT FK_E19D9AD2896DBBDE');
        $this->addSql('ALTER TABLE user_establishment DROP CONSTRAINT FK_35611CBE783E3463');
        $this->addSql('DROP SEQUENCE appointment_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE appointment_status_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE day_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE department_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE establishment_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE hours_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE report_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE review_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE service_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE type_of_shift_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE user_establishment_id_seq CASCADE');
        $this->addSql('DROP TABLE appointment');
        $this->addSql('DROP TABLE appointment_service');
        $this->addSql('DROP TABLE appointment_status');
        $this->addSql('DROP TABLE day');
        $this->addSql('DROP TABLE department');
        $this->addSql('DROP TABLE establishment');
        $this->addSql('DROP TABLE hours');
        $this->addSql('DROP TABLE report');
        $this->addSql('DROP TABLE review');
        $this->addSql('DROP TABLE service');
        $this->addSql('DROP TABLE type_of_shift');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE user_establishment');
    }
}
