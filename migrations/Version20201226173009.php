<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201226173009 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE appointment ADD deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE appointment DROP deleted');
        $this->addSql('ALTER TABLE establishment ADD deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE establishment DROP deleted');
        $this->addSql('ALTER TABLE report ADD deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE report DROP deleted');
        $this->addSql('ALTER TABLE review ADD deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE review DROP deleted');
        $this->addSql('ALTER TABLE service ADD deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE service DROP deleted');
        $this->addSql('ALTER TABLE "user" ADD deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" DROP deleted');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE "user" ADD deleted BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('ALTER TABLE "user" DROP deleted_at');
        $this->addSql('ALTER TABLE appointment ADD deleted BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('ALTER TABLE appointment DROP deleted_at');
        $this->addSql('ALTER TABLE establishment ADD deleted BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('ALTER TABLE establishment DROP deleted_at');
        $this->addSql('ALTER TABLE service ADD deleted BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('ALTER TABLE service DROP deleted_at');
        $this->addSql('ALTER TABLE report ADD deleted BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('ALTER TABLE report DROP deleted_at');
        $this->addSql('ALTER TABLE review ADD deleted BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('ALTER TABLE review DROP deleted_at');
    }
}
